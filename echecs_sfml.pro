TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra

linux-g++ | linux-g++-64 | linux-g++-32 {
    LIBS += -lsfml-graphics -lsfml-window -lsfml-system -pthread
}

win32 {
    LIBS += -L C:/SFML/lib

    CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-main -lsfml-network -lsfml-window -lsfml-system
    CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-main-d -lsfml-network-d -lsfml-window-d -lsfml-system-d

    INCLUDEPATH += C:/SFML/include
    DEPENDPATH += C:/SFML/include
}

SOURCES += \
    src/chess/HistoryElement.cpp \
    src/chess/SearchChessboard.cpp \
    src/chess/SimpleMove.cpp \
    src/chess/SimplePiece.cpp \
    src/chess/SimplePiecesImpl.cpp \
    src/chess/TreeAI.cpp \
    src/ihm/Appli.cpp \
    src/ihm/player/AI.cpp \
    src/ihm/player/Human.cpp \
    src/ihm/player/Player.cpp \
    src/ihm/ui_element/Button.cpp \
    src/ihm/ui_element/ButtonList.cpp \
    src/ihm/ui_element/Piece.cpp \
    src/ihm/ui_element/PiecesImpl.cpp \
    src/main.cpp \ 
    src/ihm/ui_element/HistoryList.cpp \
    src/ihm/ui_element/PictureElement.cpp
    
    


HEADERS += \
    src/chess/HistoryElement.h \
    src/chess/SearchChessboard.h \
    src/chess/SimpleMove.h \
    src/chess/SimplePiece.h \
    src/chess/SimplePiecesImpl.h \
    src/chess/TreeAI.h \
    src/ihm/Appli.h \
    src/ihm/player/AI.h \
    src/ihm/player/Human.h \
    src/ihm/player/IHMPlayer.h \
    src/ihm/player/Player.h \
    src/ihm/ui_element/Button.h \
    src/ihm/ui_element/ButtonList.h \
    src/ihm/ui_element/Piece.h \
    src/ihm/ui_element/PiecesImpl.h \
    src/Commun.h \ 
    src/ihm/ui_element/HistoryList.h \
    src/ihm/ui_element/PictureElement.h
