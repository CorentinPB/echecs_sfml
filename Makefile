DEBUG=	0

FOLDER=	./src/

FILE= 	\
    chess/HistoryElement.cpp \
    chess/SearchChessboard.cpp \
    chess/SimpleMove.cpp \
    chess/SimplePiece.cpp \
    chess/SimplePiecesImpl.cpp \
    chess/TreeAI.cpp \
    ihm/Appli.cpp \
    ihm/player/AI.cpp \
    ihm/player/Human.cpp \
    ihm/player/Player.cpp \
    ihm/ui_element/Button.cpp \
    ihm/ui_element/ButtonList.cpp \
    ihm/ui_element/Piece.cpp \
    ihm/ui_element/PiecesImpl.cpp \
    ihm/ui_element/HistoryList.cpp \
    ihm/ui_element/PictureElement.cpp \
    main.cpp

SRC=		$(addprefix $(FOLDER), $(FILE))

OBJ=	$(SRC:.cpp=.o)

NAME=	echec_sfml

CC=	g++

RM=	rm -rf

HEAD=	-Iinclude

LIBS= 	-lsfml-graphics -lsfml-window -lsfml-system -pthread

ifeq ($(DEBUG), 1)
	CPPFLAGS= $(HEAD) -W -Wall -Wextra -pedantic -ansi -g -Og
else
	CPPFLAGS= $(HEAD) -W -Wall -Wextra
endif


$(NAME):$(OBJ)
	$(CC) $(CPPFLAGS) $(LIBS) $(OBJ) -o $(NAME)

all:	$(NAME)

clean:
	@rm -f $(OBJ) *~ TAGS *.gcov *.gcno *.gcda *.html *.xml *.d *.o *.lcov

fclean:	clean
	@rm -f $(NAME)

re:	fclean all

.PHONY:	re all clean fclean