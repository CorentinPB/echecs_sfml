#include "TreeAI.h"
#include "../Commun.h"

/**
 * @brief TreeAI::TreeAI
 * Constructor for a TreeAI
 * @param chessboard
 */
TreeAI::TreeAI( std::shared_ptr<SearchChessboard> chessboard, int depth )
    : m_chessboard( chessboard ), m_depth(depth)
{}

/**
 * @brief search
 * The method used to launch the search of the best move
 * @return the best move it has found
 */
std::shared_ptr<SimpleMove>
TreeAI::search()
{
    // This will hold the value of the best move found by the AI
    int best = -DEFAULT_VALUE;

    // A vector that will contains all the Movement that can be made and that have
    // an evaluation value equals to best
    std::vector<std::shared_ptr<SimpleMove>> bestMoves;

    // We collect all the pieces of the actual player on the chessboard
    std::vector<unsigned int> listOfPiecesIndex = m_chessboard->getActualPlayerPieces();
    // For all of those index of pieces
    for ( unsigned int from : listOfPiecesIndex )
        // Get all possible move
        for ( unsigned int to : m_chessboard->getMoveListOf( from ) )
        {
            // We make the move
            m_chessboard->move( from, to );
            //  Then we launch a recursive search for the best move
            int result = minmax( 0, false, -( DEFAULT_VALUE+1 ), DEFAULT_VALUE+1 );
            // Once the search is complete we undo the move
            m_chessboard->undo();
            // And we save the move if its the best so far
            if ( result > best )
            {
                bestMoves.clear();
                best = result;
                bestMoves.push_back(std::make_shared<SimpleMove>(SimpleMove(from, to)));
            }
            else if (result == best)
            {
                bestMoves.push_back(std::make_shared<SimpleMove>(SimpleMove(from, to)));
            }
        }
    // Then finally we return the best move if there is only one or we select one of the moves of equals value
    if (bestMoves.size() > 0)
    {
        unsigned int selectedBestMove = static_cast<unsigned int>(rand() % static_cast<long>(bestMoves.size()));
        return bestMoves.at(selectedBestMove);
    }
    else
        return nullptr;
}

/**
 * @brief TreeAI::minmax
 * This function is a recursive tree parcour to determine the best moves
 * @param depth     The Depth we are in our recursive call
 * @param maximise  Boolean that specify if the current player must maximise or minimise
 * @param alpha     The best maximising move evaluation down the tree
 * @param beta      The best minimising move evaluation down the tree
 * @return The best Evaluation so far
 */
int
TreeAI::minmax( int depth,  bool maximise, int alpha, int beta )
{
    // First to add a limit if the depth is greater than the recursion
    // limiter we evaluate the board and return the value
    if ( depth >= m_depth ) {
        return m_chessboard->evaluateBoard((m_chessboard->isActualPlayerWhite()?maximise:!maximise));
    }

    // If not at the max depth
    // We collect all the indexes of pieces of the actual player on the chessboard
    std::vector<unsigned int> listOfPiecesIndex = m_chessboard->getActualPlayerPieces();

    // This will hold the value of the best move found by the AI for the actual player
    // One player try to get the highest value
    // The other try to get the lowest value
    int best = DEFAULT_VALUE * (maximise ? -1 : 1);

    // For all of those index of pieces
    for ( unsigned int from : listOfPiecesIndex ) {
        // Get all possible move
        for ( unsigned int to : m_chessboard->getMoveListOf( from ) )
        {
            // We make the move
            m_chessboard->move( from, to );
            // If the player is trying to get the highest value
            if (maximise)
            {
                // We save the best value between the last best value and the best move we found after this move
                best = std::max( best, minmax( depth+1, !maximise, alpha, beta ) );
                alpha = std::max( alpha, best );
            }
            // If the player is trying to get the lowest value
            else
            {
                // We save the best value between the last best value and the best move we found after this move
                best = std::min( best, minmax( depth+1, !maximise, alpha, beta ) );
                beta = std::min( beta, best );
            }
            // Once the exploration is complete we undo the move
            m_chessboard->undo();

            // If this condition is True then this part of the tree is not interesting
            if ( beta <= alpha )
                return best;
        }
    }
    return best;
}
