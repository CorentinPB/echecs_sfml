/**
 * Implementation of SimpleSimplePiece.h
 *
 * @file SimpleSimplePiece.cpp
 */
#include "SimplePiece.h"

#include "SearchChessboard.h"

#include <iostream>
#include <string>



SimplePiece::SimplePiece(unsigned int index, bool isWhite )
    : m_index( index ), m_isWhite( isWhite )
{}

SimplePiece::~SimplePiece()
{}

void
SimplePiece::move( unsigned int index )
{
    m_index = index;
}

unsigned int
SimplePiece::getIndex() const
{
    return m_index;
}

bool
SimplePiece::isWhite() const
{
    return m_isWhite;
}

bool
SimplePiece::isEaten() const
{
    return m_isEaten;
}

void
SimplePiece::eat()
{
    m_isEaten = true;
}

void SimplePiece::spit()
{
    m_isEaten = false;
}

char
SimplePiece::getChar() const
{
    return ( isWhite() )?'B':'N';
}

int
SimplePiece::getValue() const
{
    return -99999;
}

bool
SimplePiece::isCheckMove(std::shared_ptr<SearchChessboard> cb, unsigned int start, unsigned int end) {
    cb->move(start, end);
    if(cb->isCheck(this->isWhite()?0:1)) {
        cb->undo();
        return true;
    }
    cb->undo();
    return false;
}

