
/**
 * Implementation of SimplePiecesImpl.h
 *
 * @file SimplePiecesImpl.cpp
 */
#include "SimplePiecesImpl.h"

#include "SearchChessboard.h"
#include "SimplePiece.h"

#include <iostream>
#include <string>


/****** Constructors used during initialisation of the chessboard ******/

SimpleKing::SimpleKing( bool isWhite )
    : SimplePiece( isWhite?4:60, isWhite )
{}


SimplePawn::SimplePawn( bool isWhite, unsigned int pos )
    : SimplePiece( pos + (isWhite?8:48), isWhite )
{}


SimpleKnight::SimpleKnight( bool isWhite, const bool left )
    : SimplePiece( ((left?1:6) + (isWhite?0:56)), isWhite )
{}


SimpleBishop::SimpleBishop( bool isWhite, const bool left )
    : SimplePiece( ((left?2:5) + (isWhite?0:56)), isWhite )
{}


SimpleRook::SimpleRook( bool isWhite, const bool left )
    : SimplePiece( ((left?0:7) + (isWhite?0:56)), isWhite )
{}


SimpleQueen::SimpleQueen( bool isWhite )
    : SimplePiece( isWhite?3:59, isWhite ),
    SimpleBishop( isWhite, true ),
    SimpleRook( isWhite, true )
{}

/****** Constructors used to clone Pieces ******/

// SimpleKing
SimpleKing::SimpleKing( unsigned int index, bool isWhite )
    : SimplePiece( index, isWhite )
{}

// SimplePawn
SimplePawn::SimplePawn( unsigned int index, bool isWhite )
    : SimplePiece( index, isWhite )
{}

// SimpleKnight
SimpleKnight::SimpleKnight( unsigned int index, bool isWhite )
    : SimplePiece( index, isWhite )
{}

// SimpleBishop
SimpleBishop::SimpleBishop( unsigned int index, bool isWhite )
    : SimplePiece( index, isWhite )
{}

// SimpleRook
SimpleRook::SimpleRook( unsigned int index, bool isWhite )
    : SimplePiece( index, isWhite )
{}

// SimpleQueen
SimpleQueen::SimpleQueen( unsigned int index, bool isWhite )
    : SimplePiece( index, isWhite ),
    SimpleBishop( index, isWhite ),
    SimpleRook( index, isWhite )
{}

/****** Functions used to get pieces values ******/
int
SimpleKing::getValue() const
{
    return 900;
}
int
SimpleQueen::getValue() const
{
    return 90;
}
int
SimpleBishop::getValue() const
{
    return 30;
}
int
SimpleRook::getValue() const
{
    return 50;
}
int
SimpleKnight::getValue() const
{
    return 30;
}
int
SimplePawn::getValue() const
{
    return 10;
}

/****** King Methods ******/

bool
SimpleKing::validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck)
{
    if( IndexTools::isValid(index) && (cb->getSimplePiece(index) == nullptr || cb->getSimplePiece(index)->isWhite() != isWhite()) ) {
        int deltaX = IndexTools::getX(m_index) - IndexTools::getX(index);
        int deltaY = IndexTools::getY(m_index) - IndexTools::getY(index);
        if(abs(deltaX) < 2 && abs(deltaY) < 2 && m_index != index)
            return !checkIsCheck || !isCheckMove(cb, m_index, index);
    }
    return false;
}

std::vector<unsigned int>
SimpleKing::movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck ) {
    unsigned int x = IndexTools::getX(m_index);
    unsigned int y = IndexTools::getY(m_index);
    std::vector<unsigned int> list;
    for( unsigned int i = x-1; i <= x+1; ++i ) {
        if( isValid(i,y-1) && (cb->getSimplePiece(toIndex(i,y-1)) == nullptr || cb->getSimplePiece(toIndex(i,y-1))->isWhite() != isWhite() ) )
            if(!checkIsCheck || !isCheckMove(cb,m_index, toIndex(i,y-1)))
                list.push_back( toIndex(i,y-1) );
        if( isValid(i,y+1) && (cb->getSimplePiece(toIndex(i,y+1)) == nullptr || cb->getSimplePiece(toIndex(i,y+1))->isWhite() != isWhite() ) )
            if(!checkIsCheck || !isCheckMove(cb,m_index,  toIndex(i,y+1)))
                list.push_back( toIndex(i,y+1) );
    }
    if( isValid(x+1,y) && (cb->getSimplePiece(toIndex(x+1,y)) == nullptr || cb->getSimplePiece(toIndex(x+1,y))->isWhite() != isWhite() ) )
        if(!checkIsCheck || !isCheckMove(cb,m_index, toIndex(x+1,y)))
            list.push_back( toIndex(x+1,y) );
    if( isValid(x-1,y) && (cb->getSimplePiece(toIndex(x-1,y)) == nullptr || cb->getSimplePiece(toIndex(x-1,y))->isWhite() != isWhite() ) )
        if(!checkIsCheck || !isCheckMove(cb,m_index, toIndex(x-1,y)))
            list.push_back( toIndex(x-1,y) );
    return list;
}


char
SimpleKing::getChar() const
{
    return 'r';
}


/****** Pawn Methods ******/

bool
SimplePawn::validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck )
{
    unsigned int mx = IndexTools::getX(m_index);
    unsigned int my = IndexTools::getY(m_index);
    unsigned int ix = IndexTools::getX(index);
    unsigned int iy = IndexTools::getY(index);
    if( IndexTools::isValid(index) && (cb->getSimplePiece(index) == nullptr || cb->getSimplePiece(index)->isWhite() != m_isWhite) ) {
        if(isWhite()) {
            if(my == 1 && mx == ix && my+2 == iy && cb->getSimplePiece(index) == nullptr && cb->getSimplePiece(toIndex(ix, iy-1)) == nullptr)
                return !checkIsCheck || !isCheckMove(cb, m_index, index);
            if(mx == ix && my+1 == iy && cb->getSimplePiece(index) == nullptr)
                return !checkIsCheck || !isCheckMove(cb, m_index, index);
            if(abs(mx - ix) == 1 && my+1 == iy && cb->getSimplePiece(index) != nullptr && !cb->getSimplePiece(index)->isWhite())
                return !checkIsCheck || !isCheckMove(cb, m_index, index);
        }
        else {
            if(my == 6 && mx == ix && my-2 == iy && cb->getSimplePiece(index) == nullptr && cb->getSimplePiece(toIndex(ix, iy+1)) == nullptr)
                return !checkIsCheck || !isCheckMove(cb, m_index, index);
            if(mx == ix && my-1 == iy && cb->getSimplePiece(index) == nullptr)
                return !checkIsCheck || !isCheckMove(cb, m_index, index);
            if(abs(mx - ix) == 1 && my-1 == iy && cb->getSimplePiece(index) != nullptr && cb->getSimplePiece(index)->isWhite())
                return !checkIsCheck || !isCheckMove(cb, m_index, index);
        }
    }
    return false;
}

std::vector<unsigned int>
SimplePawn::movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck ) {
    std::vector<unsigned int> list;
    if(isWhite()) {
        if(IndexTools::isValid(m_index+8) && cb->getSimplePiece(m_index+8) == nullptr && (!checkIsCheck || !isCheckMove(cb,m_index,  m_index+8))) {
            list.push_back( m_index+8 );
            if(IndexTools::getY(m_index) == 1 && IndexTools::isValid(m_index+16) && cb->getSimplePiece(m_index+16) == nullptr)
                if(!checkIsCheck || !isCheckMove(cb,m_index, m_index+16))
                    list.push_back( m_index+16 );
        }
        if(IndexTools::getY(m_index) != 7 && IndexTools::getX(m_index) != 7
                && cb->getSimplePiece(m_index+9) != nullptr && !cb->getSimplePiece(m_index+9)->isWhite())
            if(!checkIsCheck || !isCheckMove(cb,m_index, m_index+9))
                list.push_back( m_index+9 );
        if(IndexTools::getY(m_index) != 7 && IndexTools::getX(m_index) != 0
                && cb->getSimplePiece(m_index+7) != nullptr && !cb->getSimplePiece(m_index+7)->isWhite())
            if(!checkIsCheck || !isCheckMove(cb,m_index, m_index+7))
                list.push_back( m_index+7 );
    }
    else {
        if(IndexTools::isValid(m_index-8) && cb->getSimplePiece(m_index-8) == nullptr && (!checkIsCheck || !isCheckMove(cb,m_index,  m_index-8))) {
            list.push_back( m_index-8 );
            if(IndexTools::getY(m_index) == 6 && IndexTools::isValid(m_index-16) && cb->getSimplePiece(m_index-16) == nullptr)
                if(!checkIsCheck || !isCheckMove(cb,m_index,  m_index-16))
                    list.push_back( m_index-16 );
        }
        if(IndexTools::getY(m_index) != 0 && IndexTools::getX(m_index) != 0
                && cb->getSimplePiece(m_index-9) != nullptr && cb->getSimplePiece(m_index-9)->isWhite())
            if(!checkIsCheck || !isCheckMove(cb,m_index,  m_index-9))
                list.push_back( m_index-9 );
        if(IndexTools::getY(m_index) != 0 && IndexTools::getX(m_index) != 7
                && cb->getSimplePiece(m_index-7) != nullptr && cb->getSimplePiece(m_index-7)->isWhite())
            if(!checkIsCheck || !isCheckMove(cb,m_index,  m_index-7))
                list.push_back( m_index-7 );
    }
    return list;
}


char
SimplePawn::getChar() const
{
    return 'p';
}


/****** Knight Methods ******/

bool
SimpleKnight::validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck)
{
    if( IndexTools::isValid(index) && (cb->getSimplePiece(index) == nullptr || cb->getSimplePiece(index)->isWhite() != isWhite()) ) {
        int deltaX = abs(IndexTools::getX(m_index) - IndexTools::getX(index));
        int deltaY = abs(IndexTools::getY(m_index) - IndexTools::getY(index));
        return deltaY < 3 && deltaX < 3 && deltaX+deltaY == 3 && (!checkIsCheck || !isCheckMove(cb, m_index, index));
    }
    return false;
}

std::vector<unsigned int>
SimpleKnight::movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck ) {
    unsigned int positions[] = { 17, 15, 10, 6 };
    std::vector<unsigned int> list;
    for (unsigned int index : positions) {
        if( validMovement(cb, m_index+index, false))
            if(!checkIsCheck || !isCheckMove(cb,m_index, m_index+index))
                list.push_back(m_index+index);
        if( validMovement(cb, m_index-index, false))
            if(!checkIsCheck || !isCheckMove(cb,m_index, m_index-index))
                list.push_back(m_index-index);
    }
    return list;
}

char
SimpleKnight::getChar() const
{
    return 'c';
}

/****** Bishop Methods ******/

bool
SimpleBishop::validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck)
{
    if (m_index == index)
        return false;

    if( IndexTools::isValid(index) && (cb->getSimplePiece(index) == nullptr || cb->getSimplePiece(index)->isWhite() != isWhite()) ) {
        unsigned int mx = IndexTools::getX(m_index);
        unsigned int my = IndexTools::getY(m_index);
        unsigned int ix = IndexTools::getX(index);
        unsigned int iy = IndexTools::getY(index);

        int deltaX = static_cast<int>(mx) - static_cast<int>(ix);
        int deltaY = static_cast<int>(my) - static_cast<int>(iy);
        if(std::abs(deltaX) == std::abs(deltaY)) {
            int k = (deltaX>0)?-1:1;
            int l = (deltaY>0)?-1:1;
            int i = static_cast<int>(mx) + k;
            int j = static_cast<int>(my) + l;
            while(i >= 0 && j >= 0 && i < 8 && j < 8 && i != static_cast<int>(ix) && j != static_cast<int>(iy)) {
                if(cb->getSimplePiece(toIndex(static_cast<unsigned int>(i), static_cast<unsigned int>(j))) != nullptr)
                    return false;
                i+=k;
                j+=l;
            }
            return !checkIsCheck || !isCheckMove(cb, m_index, index);
        }
    }
    return false;
}

std::vector<unsigned int>
SimpleBishop::movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck ) {
    std::vector<unsigned int> list;
    unsigned int i = 1;
    unsigned int tmp_index = m_index + (i*8 + i);
    bool stop = false;
    while(!stop && IndexTools::getX(m_index) != 7 && IndexTools::getY(m_index) != 7
          && IndexTools::isValid(tmp_index) ) {
        if(cb->getSimplePiece(tmp_index) == nullptr) {
            if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                list.push_back(tmp_index);
        } else {
            if(cb->getSimplePiece(tmp_index)->isWhite() != isWhite())
                if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                    list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 7 || IndexTools::getY(tmp_index) == 7 )
            stop = true;
        ++i;
        tmp_index = m_index + (i*8 + i);
    }
    i = 1;
    tmp_index = m_index + (i*8 - i);
    stop = false;
    while(!stop && IndexTools::getX(m_index) != 0 && IndexTools::getY(m_index) != 7
          && IndexTools::isValid(tmp_index) ) {
        if(cb->getSimplePiece(tmp_index) == nullptr) {
            if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                list.push_back(tmp_index);
        } else {
            if(cb->getSimplePiece(tmp_index)->isWhite() != isWhite())
                if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                    list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 0 || IndexTools::getY(tmp_index) == 7 )
            stop = true;
        ++i;
        tmp_index = m_index + (i*8 - i);
    }
    i = 1;
    tmp_index = m_index + (i*-8 + i);
    stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getX(m_index) != 7
          && IndexTools::getY(m_index) != 0) {
        if(cb->getSimplePiece(tmp_index) == nullptr) {
            if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                list.push_back(tmp_index);
        } else {
            if(cb->getSimplePiece(tmp_index)->isWhite() != isWhite())
                if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                    list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 7 || IndexTools::getY(tmp_index) == 0 )
            stop = true;
        ++i;
        tmp_index = m_index + (i*-8 + i);
    }
    i = 1;
    tmp_index = m_index + (i*-8 - i);
    stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getX(m_index) != 0
          && IndexTools::getY(m_index) != 0 ) {
        if(cb->getSimplePiece(tmp_index) == nullptr) {
            if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                list.push_back(tmp_index);
        } else {
            if(cb->getSimplePiece(tmp_index)->isWhite() != isWhite())
                if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                    list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 0 || IndexTools::getY(tmp_index) == 0 )
            stop = true;
        ++i;
        tmp_index = m_index + (i*-8 - i);
    }
    return list;
}


char
SimpleBishop::getChar() const
{
    return 'f';
}

/****** Rook Methods ******/

bool
SimpleRook::validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck)
{
    if( IndexTools::isValid(index) && (cb->getSimplePiece(index) == nullptr || cb->getSimplePiece(index)->isWhite() != isWhite()) ) {
        unsigned int mx = IndexTools::getX(m_index);
        unsigned int my = IndexTools::getY(m_index);
        unsigned int ix = IndexTools::getX(index);
        unsigned int iy = IndexTools::getY(index);

        if( mx == ix) {
            int j = (iy > my)?1:-1;
            for(unsigned int i = my+j;i!=iy;i+=j) {
                if(i < 8 && cb->getSimplePiece(toIndex(ix, i)) != nullptr)
                    return false;
            }
            return !checkIsCheck || !isCheckMove(cb, m_index, index);
        }
        else if (my == iy) {
            int j = (ix > mx)?1:-1;
            for(unsigned int i = mx+j;i!=ix;i+=j) {
                if(i < 8 && cb->getSimplePiece(toIndex(i, iy)) != nullptr)
                    return false;
            }
            return !checkIsCheck || !isCheckMove(cb, m_index, index);
        }
    }
    return false;
}


std::vector<unsigned int>
SimpleRook::movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck ) {
    std::vector<unsigned int> list;
    unsigned int i = 1;
    unsigned int tmp_index = m_index + i;
    bool stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getX(m_index) != 7) {
        if(cb->getSimplePiece(tmp_index) == nullptr) {
            if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                list.push_back(tmp_index);
        } else {
            if(cb->getSimplePiece(tmp_index)->isWhite() != isWhite())
                if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                    list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 7)
            stop = true;
        ++i;
        tmp_index = m_index + i;
    }
    i = 1;
    tmp_index = m_index - i;
    stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getX(m_index) != 0) {
        if(cb->getSimplePiece(tmp_index) == nullptr) {
            if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                list.push_back(tmp_index);
        } else {
            if(cb->getSimplePiece(tmp_index)->isWhite() != isWhite())
                if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                    list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 0)
            stop = true;
        ++i;
        tmp_index = m_index - i;
    }
    i = 1;
    tmp_index = m_index + (i*-8);
    stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getY(m_index) != 0) {
        if(cb->getSimplePiece(tmp_index) == nullptr) {
            if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                list.push_back(tmp_index);
        } else {
            if(cb->getSimplePiece(tmp_index)->isWhite() != isWhite())
                if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                    list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getY(tmp_index) == 0 )
            stop = true;
        ++i;
        tmp_index = m_index + (i*-8);
    }
    i = 1;
    tmp_index = m_index + (i*8);
    stop = false;
    while(!stop && IndexTools::getY(m_index) != 7 && IndexTools::isValid(tmp_index)) {
        if(cb->getSimplePiece(tmp_index) == nullptr) {
            if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                list.push_back(tmp_index);
        } else {
            if(cb->getSimplePiece(tmp_index)->isWhite() != isWhite())
                if(!checkIsCheck || !isCheckMove(cb,m_index, tmp_index))
                    list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getY(tmp_index) == 7)
            stop = true;
        ++i;
        tmp_index = m_index + (i*8);
    }
    return list;
}


char
SimpleRook::getChar() const
{
    return 't';
}


/****** Queen Methods ******/

bool
SimpleQueen::validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck )
{
    return SimpleBishop::validMovement(cb,index, checkIsCheck) || SimpleRook::validMovement(cb,index, checkIsCheck);
}


std::vector<unsigned int>
SimpleQueen::movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck ) {
    std::vector<unsigned int> list;
    std::vector<unsigned int> list_fou = SimpleBishop::movementsIndexList(cb, checkIsCheck);
    std::vector<unsigned int> list_tour = SimpleRook::movementsIndexList(cb, checkIsCheck);
    list.insert( list.end(), list_fou.begin(), list_fou.end() );
    list.insert( list.end(), list_tour.begin(), list_tour.end() );
    return list;
}


char
SimpleQueen::getChar() const
{
    return 'q';
}
