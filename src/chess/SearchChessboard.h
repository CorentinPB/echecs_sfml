#ifndef SEARCHCHESSBOARD_H
#define SEARCHCHESSBOARD_H

#include "../ihm/player/Player.h"
#include "SimplePiece.h"
#include "HistoryElement.h"

#include <array>
#include <vector>
#include <memory>


class SearchChessboard
        : public std::enable_shared_from_this<SearchChessboard>
{
public:
    SearchChessboard( bool nextPlayerIsWhite );

    std::shared_ptr<SimplePiece> getSimplePiece( unsigned int index ) const;

    std::vector<unsigned int> getActualPlayerPieces();

    std::vector<unsigned int> getMoveListOf( unsigned int index );

    void move(unsigned int from, unsigned int to );
    void place( std::shared_ptr<SimplePiece> p );

    void undo();

    unsigned int getActualPlayer();
    bool isActualPlayerWhite();
    std::shared_ptr<HistoryElement> getLastHistoryElement();

    bool isCheck(unsigned int playerToTest);
    unsigned int isCheckmateOrStalemate(unsigned int playerToTest);

    int evaluateBoard(bool maximisingPlayerIsWhite);

    void show();

private:
    std::array<std::shared_ptr<SimplePiece>, CHESSBOARD_SIZE> m_tiles;
    std::vector<std::shared_ptr<HistoryElement>> m_history;

    std::vector<std::shared_ptr<SimplePiece>> m_whitePieces;
    std::vector<std::shared_ptr<SimplePiece>> m_blackPieces;

    bool m_nextPlayerIsWhite;
};

#endif // SEARCHCHESSBOARD_H
