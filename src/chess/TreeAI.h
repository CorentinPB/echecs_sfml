#ifndef TREEAI_H
#define TREEAI_H

#include <memory>
#include <vector>
#include <algorithm>    // std::min & std::max
#include <stdlib.h>     // rand

#include "SearchChessboard.h"
#include "SimpleMove.h"


class TreeAI
{
private:
    std::shared_ptr<SearchChessboard> m_chessboard;
    int m_depth;

    /**
     * @brief TreeAI::minmax
     * This function is a recursive tree parcour to determine the best moves
     * @param depth     The Depth we are in our recursive call
     * @param maximise  Boolean that specify if the current player must maximise or minimise
     * @param alpha     The best maximising move evaluation down the tree
     * @param beta      The best minimising move evaluation down the tree
     * @return The best Evaluation so far
     */
    int minmax(int depth,  bool maximise, int alpha, int beta);
public:
    /**
     * @brief TreeAI::TreeAI
     * Constructor for a TreeAI
     * @param chessboard
     */
    TreeAI(std::shared_ptr<SearchChessboard> chessboard, int depth=DEPTH);

    /**
     * @brief search
     * The method used to launch the search of the best move
     * @return the best move it has found
     */
    std::shared_ptr<SimpleMove> search();
};

#endif // TREEAI_H
