#include "HistoryElement.h"

HistoryElement::HistoryElement(unsigned int from, unsigned int to, std::shared_ptr<SimplePiece> eaten)
    : m_from(from), m_to(to), m_eaten(eaten)
{}

unsigned int
HistoryElement::getFrom()
{
    return m_from;
}

unsigned int
HistoryElement::getTo()
{
    return m_to;
}

std::shared_ptr<SimplePiece>
HistoryElement::getEaten()
{
    return m_eaten;
}
