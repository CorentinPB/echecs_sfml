#ifndef SIMPLEMOVE_H
#define SIMPLEMOVE_H


class SimpleMove
{
private:
    unsigned int m_from;
    unsigned int m_to;
public:
    SimpleMove(unsigned int from, unsigned int to);

    unsigned int from();
    unsigned int to();
};

#endif // SIMPLEMOVE_H
