#include "SearchChessboard.h"

#include <iostream>

#include "SimplePiecesImpl.h"

SearchChessboard::SearchChessboard( bool nextPlayerIsWhite )
    : m_nextPlayerIsWhite(nextPlayerIsWhite)
{
    for (unsigned int i = 0; i < CHESSBOARD_SIZE; i++)
        m_tiles[i] = nullptr;
}

std::shared_ptr<SimplePiece>
SearchChessboard::getSimplePiece( unsigned int index ) const
{
    return m_tiles[index];
}

std::vector<unsigned int>
SearchChessboard::getActualPlayerPieces()
{
    std::vector<unsigned int> piecesIndex;
    for (std::shared_ptr<SimplePiece> p : (m_nextPlayerIsWhite ? m_whitePieces : m_blackPieces))
        if (!p->isEaten())
            piecesIndex.push_back(p->getIndex());
    return piecesIndex;
}

std::vector<unsigned int>
SearchChessboard::getMoveListOf(unsigned int index)
{
    return m_tiles[index]->movementsIndexList(shared_from_this());
}

void
SearchChessboard::move(unsigned int from, unsigned int to )
{
    m_nextPlayerIsWhite = !m_nextPlayerIsWhite;
    m_history.push_back(std::make_shared<HistoryElement>(HistoryElement(from, to, m_tiles[to])));
    if (m_tiles[to] != nullptr) {
        m_tiles[to]->eat();
    }
    m_tiles[from]->move(to);
    m_tiles[to] = m_tiles[from];
    m_tiles[from] = nullptr;
}

void
SearchChessboard::place(std::shared_ptr<SimplePiece> p )
{
    if (p->isWhite())
        m_whitePieces.push_back(p);
    else
        m_blackPieces.push_back(p);
    m_tiles[p->getIndex()] = p;
}

void
SearchChessboard::undo()
{
    m_nextPlayerIsWhite = !m_nextPlayerIsWhite;
    std::shared_ptr<HistoryElement> elem = m_history.back();
    m_tiles[elem->getTo()]->move(elem->getFrom());
    m_tiles[elem->getFrom()] = m_tiles[elem->getTo()];
    if (elem->getEaten() != nullptr)
        elem->getEaten()->spit();
    m_tiles[elem->getTo()] = elem->getEaten();
    m_history.pop_back();
}

unsigned int SearchChessboard::getActualPlayer()
{
    return m_nextPlayerIsWhite ? 0 : 1;
}

bool SearchChessboard::isActualPlayerWhite()
{
    return m_nextPlayerIsWhite;
}

std::shared_ptr<HistoryElement> SearchChessboard::getLastHistoryElement() {
    if (m_history.size() > 0) {
        return m_history.back();
    } else {
        return nullptr;
    }
}

unsigned int SearchChessboard::isCheckmateOrStalemate(unsigned int playerToTest) {
    std::vector<std::shared_ptr<SimplePiece>> pieces = (playerToTest == 0)?m_whitePieces:m_blackPieces;
    for(unsigned int i = 0; i < pieces.size(); ++i) {
        if(!pieces[i]->isEaten() && pieces[i]->movementsIndexList(shared_from_this()).size() != 0)
            return 0; // NOTHING
    }
    if(isCheck(playerToTest))
        return 2; // CHECKMATE
    return 1; // STALEMATE
}

int
SearchChessboard::evaluateBoard(bool maximisingPlayerIsWhite)
{
    int eval = 0;
    for (unsigned int i = 0; i < CHESSBOARD_SIZE; i++)
        if (m_tiles[i] != nullptr)
        {
            if (maximisingPlayerIsWhite == m_tiles[i]->isWhite())
                eval += m_tiles[i]->getValue();
            else
                eval -= m_tiles[i]->getValue();
        }
    return eval;
}

void
SearchChessboard::show()
{
    std::cout << "----------------" << std::endl;
    for (unsigned int i = 0; i < CHESSBOARD_LINE_SIZE; i++)
    {
        for (unsigned int j = 0; j < CHESSBOARD_LINE_SIZE; j++)
        {
            if (m_tiles[toIndex(j, i)] != nullptr)
                std::cout << " " << m_tiles[toIndex(j, i)]->getChar();
            else
                std::cout << "  ";
        }
        std::cout << std::endl;
    }
    std::cout << "----------------" << std::endl;
}

bool
SearchChessboard::isCheck(unsigned int playerToTest) {
    std::shared_ptr<SimplePiece> king = (playerToTest == 0)?m_whitePieces[0]:m_blackPieces[0];
    unsigned int kingIndex = king->getIndex();
    bool kingIsWhite = king->isWhite();

    std::vector<unsigned int> list;

    // ROOK
    unsigned int i = 1;
    unsigned int tmp_index = kingIndex + i;
    bool stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getX(kingIndex) != 7) {
        if(getSimplePiece(tmp_index) == nullptr) {
            list.push_back(tmp_index);
        } else {
            if(getSimplePiece(tmp_index)->isWhite() != kingIsWhite)
                list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 7)
            stop = true;
        ++i;
        tmp_index = kingIndex + i;
    }
    i = 1;
    tmp_index = kingIndex - i;
    stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getX(kingIndex) != 0) {
        if(getSimplePiece(tmp_index) == nullptr) {
            list.push_back(tmp_index);
        } else {
            if(getSimplePiece(tmp_index)->isWhite() != kingIsWhite)
                list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 0)
            stop = true;
        ++i;
        tmp_index = kingIndex - i;
    }
    i = 1;
    tmp_index = kingIndex + (i*-8);
    stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getY(kingIndex) != 0) {
        if(getSimplePiece(tmp_index) == nullptr) {
            list.push_back(tmp_index);
        } else {
            if(getSimplePiece(tmp_index)->isWhite() != kingIsWhite)
                list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getY(tmp_index) == 0 )
            stop = true;
        ++i;
        tmp_index = kingIndex + (i*-8);
    }
    i = 1;
    tmp_index = kingIndex + (i*8);
    stop = false;
    while(!stop && IndexTools::getY(kingIndex) != 7 && IndexTools::isValid(tmp_index)) {
        if(getSimplePiece(tmp_index) == nullptr) {
            list.push_back(tmp_index);
        } else {
            if(getSimplePiece(tmp_index)->isWhite() != kingIsWhite)
                list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getY(tmp_index) == 7)
            stop = true;
        ++i;
        tmp_index = kingIndex + (i*8);
    }

    // BISHOP
    i = 1;
    tmp_index = kingIndex + (i*8 + i);
    stop = false;
    while(!stop && IndexTools::getX(kingIndex) != 7 && IndexTools::getY(kingIndex) != 7
          && IndexTools::isValid(tmp_index) ) {
        if(getSimplePiece(tmp_index) == nullptr) {
            list.push_back(tmp_index);
        } else {
            if(getSimplePiece(tmp_index)->isWhite() != kingIsWhite)
                list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 7 || IndexTools::getY(tmp_index) == 7 )
            stop = true;
        ++i;
        tmp_index = kingIndex + (i*8 + i);
    }
    i = 1;
    tmp_index = kingIndex + (i*8 - i);
    stop = false;
    while(!stop && IndexTools::getX(kingIndex) != 0 && IndexTools::getY(kingIndex) != 7
          && IndexTools::isValid(tmp_index) ) {
        if(getSimplePiece(tmp_index) == nullptr) {
            list.push_back(tmp_index);
        } else {
            if(getSimplePiece(tmp_index)->isWhite() != kingIsWhite)
                list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 0 || IndexTools::getY(tmp_index) == 7 )
            stop = true;
        ++i;
        tmp_index = kingIndex + (i*8 - i);
    }
    i = 1;
    tmp_index = kingIndex + (i*-8 + i);
    stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getX(kingIndex) != 7
          && IndexTools::getY(kingIndex) != 0) {
        if(getSimplePiece(tmp_index) == nullptr) {
            list.push_back(tmp_index);
        } else {
            if(getSimplePiece(tmp_index)->isWhite() != kingIsWhite)
                list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 7 || IndexTools::getY(tmp_index) == 0 )
            stop = true;
        ++i;
        tmp_index = kingIndex + (i*-8 + i);
    }
    i = 1;
    tmp_index = kingIndex + (i*-8 - i);
    stop = false;
    while(!stop && IndexTools::isValid(tmp_index) && IndexTools::getX(kingIndex) != 0
          && IndexTools::getY(kingIndex) != 0 ) {
        if(getSimplePiece(tmp_index) == nullptr) {
            list.push_back(tmp_index);
        } else {
            if(getSimplePiece(tmp_index)->isWhite() != kingIsWhite)
                list.push_back(tmp_index);
            stop = true;
        }
        if( IndexTools::getX(tmp_index) == 0 || IndexTools::getY(tmp_index) == 0 )
            stop = true;
        ++i;
        tmp_index = kingIndex + (i*-8 - i);
    }

    // KNIGHT
    unsigned int positions[] = { 17, 15, 10, 6 };

    for (unsigned int index : positions) {
        if( IndexTools::isValid(kingIndex+index) && (getSimplePiece(kingIndex+index) == nullptr || getSimplePiece(kingIndex+index)->isWhite() != kingIsWhite) ) {
            int deltaX = abs(IndexTools::getX(kingIndex) - IndexTools::getX(kingIndex+index));
            int deltaY = abs(IndexTools::getY(kingIndex) - IndexTools::getY(kingIndex+index));
            if( deltaY < 3 && deltaX < 3 && deltaX+deltaY == 3) {
                list.push_back(kingIndex+index);
            }
        }
        if( IndexTools::isValid(kingIndex-index) && (getSimplePiece(kingIndex-index) == nullptr || getSimplePiece(kingIndex-index)->isWhite() != kingIsWhite) ) {
            int deltaX = abs(IndexTools::getX(kingIndex) - IndexTools::getX(kingIndex-index));
            int deltaY = abs(IndexTools::getY(kingIndex) - IndexTools::getY(kingIndex-index));
            if( deltaY < 3 && deltaX < 3 && deltaX+deltaY == 3) {
                list.push_back(kingIndex-index);
            }
        }
    }

    i = 0;
    while(i<list.size()) {
        if(getSimplePiece(list[i]) != nullptr && getSimplePiece(list[i])->validMovement(shared_from_this(), kingIndex, false))
            return true;
        else ++i;
    }
    return false;
}
