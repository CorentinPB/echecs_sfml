/**
 * Header de SimplePiece.cpp
 *
 * @file SimplePiece.h
 */
#ifndef SIMPLEPIECE_H
#define SIMPLEPIECE_H

#include "../Commun.h"

#include <vector>
#include <memory>

class SearchChessboard;

class SimplePiece
{
protected:
    unsigned int m_index;
    bool m_isWhite;
    bool m_isEaten;

    SimplePiece( unsigned int index, bool isWhite );

public:
    virtual ~SimplePiece();

    void move(unsigned int index );

    unsigned int getIndex() const;

    bool isWhite() const;
    bool isEaten() const;

    void eat();
    void spit/*OnHim*/();

    virtual bool validMovement( std::shared_ptr<SearchChessboard> cb, unsigned int index,  bool checkIsCheck=true)=0;

    virtual std::vector<unsigned int> movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck=true)=0;

    virtual char getChar() const;
    virtual int getValue() const;

    virtual bool isCheckMove(std::shared_ptr<SearchChessboard> cb, unsigned int start, unsigned int end);
};

#endif // SIMPLEPIECE_H
