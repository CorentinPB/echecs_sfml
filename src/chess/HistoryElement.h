#ifndef HISTORYELEMENT_H
#define HISTORYELEMENT_H

#include "SimplePiece.h"

#include <memory>


class HistoryElement
{
private:
    unsigned int m_from;
    unsigned int m_to;
    std::shared_ptr<SimplePiece> m_eaten;
public:
    HistoryElement(unsigned int from, unsigned int to, std::shared_ptr<SimplePiece> eaten);

    unsigned int getFrom();
    unsigned int getTo();
    std::shared_ptr<SimplePiece> getEaten();
};

#endif // HISTORYELEMENT_H
