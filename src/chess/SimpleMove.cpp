#include "SimpleMove.h"

SimpleMove::SimpleMove(unsigned int from, unsigned int to)
    : m_from(from), m_to(to)
{}

unsigned int SimpleMove::from()
{
    return m_from;
}

unsigned int SimpleMove::to()
{
    return m_to;
}
