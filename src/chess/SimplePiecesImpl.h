/**
 * Header de SimpleSimplePiecesImpl.cpp
 *
 * @file SimpleSimplePiecesImpl.h
 */
#ifndef SIMPLEPIECEIMPL_H
#define SIMPLEPIECEIMPL_H

class Chessboard;

#include "../Commun.h"
#include "SimplePiece.h"

class SimpleKing : public SimplePiece
{
public:
    explicit SimpleKing( bool isWhite );
    explicit SimpleKing( unsigned int index, bool isWhite );

    bool validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck=true);

    std::vector<unsigned int> movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck=true  );

    char getChar() const;
    int getValue() const;
};

class SimplePawn : public SimplePiece
{
public:
    SimplePawn(bool isWhite, unsigned int pos );
    SimplePawn( unsigned int index, bool isWhite );

    bool validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck=true);

    std::vector<unsigned int> movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck=true  );

    char getChar() const;
    int getValue() const;
};

class SimpleKnight : public SimplePiece
{
public:
    SimpleKnight( bool isWhite, bool left );
    SimpleKnight( unsigned int index, bool isWhite );

    bool validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck=true);

    std::vector<unsigned int> movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck=true  );

    char getChar() const;
    int getValue() const;
};

class SimpleBishop : virtual public SimplePiece
{
public:
    SimpleBishop( bool isWhite, bool left );
    SimpleBishop( unsigned int index, bool isWhite );

    bool validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck=true) override;

    std::vector<unsigned int> movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck=true  ) override;

    char getChar() const override;
    int getValue() const override;
};

class SimpleRook : virtual public SimplePiece
{
public:
    SimpleRook( bool isWhite, bool left );
    SimpleRook( unsigned int index, bool isWhite );

    bool validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck=true);

    std::vector<unsigned int> movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck=true  );

    char getChar() const;
    int getValue() const;
};

class SimpleQueen : public SimpleBishop, public SimpleRook
{
public:
    explicit SimpleQueen( bool isWhite );
    explicit SimpleQueen( unsigned int index, bool isWhite );

    bool validMovement( std::shared_ptr<SearchChessboard> cb , unsigned int index,  bool checkIsCheck=true);

    std::vector<unsigned int> movementsIndexList( std::shared_ptr<SearchChessboard> cb,  bool checkIsCheck=true  );

    char getChar() const;
    int getValue() const;
};

#endif // SIMPLEPIECEIMPL_H
