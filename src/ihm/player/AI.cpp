#include "AI.h"
#include "AI.h"
#include "../../chess/TreeAI.h"

#include <memory>
#include <iostream>

AI::AI() {
    m_depth = 1;
}

AI::AI(int depth) {
    m_depth = depth;
}

void AI::processBestMove(std::shared_ptr<SearchChessboard> cb)
{
    GLOBAL_MUTEX.lock();
    TreeAI treeAI (cb,m_depth);
    std::shared_ptr<SimpleMove> tmp_move = treeAI.search();
    m_mutex.lock();
    m_move = tmp_move;
    m_isThreadRunning = false;
    m_mutex.unlock();
    GLOBAL_MUTEX.unlock();
}

std::shared_ptr<SimpleMove> AI::getNextMove(std::shared_ptr<SearchChessboard> cb)
{
    m_mutex.lock();
    std::shared_ptr<SimpleMove> tmp_move = m_move;
    m_mutex.unlock();
    if (!m_isThreadRunning && m_move == nullptr)
    {
        if (m_thread != nullptr && m_thread->joinable())
            m_thread->join();
        m_thread = std::make_shared<std::thread>(std::thread(&AI::processBestMove, this, cb));
        m_mutex.lock();
        m_isThreadRunning = true;
        m_mutex.unlock();
    }
    return tmp_move;
}

void AI::moveEnded()
{
    if (m_thread != nullptr && m_thread->joinable())
        m_thread->join();
    m_mutex.lock();
    m_move = nullptr;
    m_mutex.unlock();
}

bool AI::isHuman()
{
    return false;
}
