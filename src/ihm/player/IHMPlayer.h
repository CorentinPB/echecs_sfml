#ifndef IHMPLAYER_H
#define IHMPLAYER_H

#include "../../chess/SearchChessboard.h"
#include "../../chess/SimpleMove.h"

class IHMPlayer
{
public:
    virtual std::shared_ptr<SimpleMove> getNextMove(std::shared_ptr<SearchChessboard> cb) = 0;
    virtual void moveEnded() = 0;
    virtual bool isHuman() = 0;
};

#endif // IHMPLAYER_H
