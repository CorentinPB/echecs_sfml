/** 
 * Implementation of Player.h
 *
 * @file Player.cpp
 */
#include "Player.h"
#include "../../chess/SearchChessboard.h"

#include <iostream>
#include <assert.h>



Player::Player(const Color &white)
{
    std::shared_ptr<Piece> piece = std::make_shared<King>(King(white));
    m_pieces.push_back(piece);
    m_simplePieces.push_back(piece->getSimplePiece());
    piece = std::make_shared<Queen>(Queen(white));
    m_pieces.push_back(piece);
    m_simplePieces.push_back(piece->getSimplePiece());
    piece = std::make_shared<Bishop>(Bishop(white,true));
    m_pieces.push_back(piece);
    m_simplePieces.push_back(piece->getSimplePiece());
    piece = std::make_shared<Bishop>(Bishop(white,false));
    m_pieces.push_back(piece);
    m_simplePieces.push_back(piece->getSimplePiece());
    piece = std::make_shared<Knight>(Knight(white,true));
    m_pieces.push_back(piece);
    m_simplePieces.push_back(piece->getSimplePiece());
    piece = std::make_shared<Knight>(Knight(white,false));
    m_pieces.push_back(piece);
    m_simplePieces.push_back(piece->getSimplePiece());
    piece = std::make_shared<Rook>(Rook(white,true));
    m_pieces.push_back(piece);
    m_simplePieces.push_back(piece->getSimplePiece());
    piece = std::make_shared<Rook>(Rook(white,false));
    m_pieces.push_back(piece);
    m_simplePieces.push_back(piece->getSimplePiece());

    for (unsigned int x=0; x < CHESSBOARD_LINE_SIZE; x++) {
        piece = std::make_shared<Pawn>(Pawn(white,x));
        m_pieces.push_back(piece);
        m_simplePieces.push_back(piece->getSimplePiece());
    }
}

WhitePlayer::WhitePlayer() : Player(Color::colorWhite)
{

}

BlackPlayer::BlackPlayer() : Player(Color::colorBlack)
{

}


Player::~Player()
{

}

bool
WhitePlayer::isWhite()
{
    return true;
}

bool
BlackPlayer::isWhite()
{
    return false;
}


std::vector<std::shared_ptr<Piece>> Player::getPieces() {
    return m_pieces;
}

std::vector<std::shared_ptr<SimplePiece>> Player::getSimplePieces() {
    return m_simplePieces;
}

void
Player::show() const
{
    for (auto &p : m_pieces) // boucle C++11
        p->affiche();
}

void
Player::placePieces(std::shared_ptr<SearchChessboard> e)
{
    for (std::shared_ptr<Piece> &p : m_pieces) { // boucle C++11
        std::shared_ptr<SimplePiece> tmp_sp = p->getSimplePiece();
        e->place(tmp_sp);
    }
}

void Player::deletePieces(std::shared_ptr<Piece> piece) {
    std::vector<std::shared_ptr<Piece>>::iterator ite = std::find(m_pieces.begin(), m_pieces.end(), piece);
    if( ite != m_pieces.end()) {
        m_pieces.erase(ite);
    }
}
