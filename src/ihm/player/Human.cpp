#include "Human.h"

Human::Human()
{}


std::shared_ptr<SimpleMove>
Human::getNextMove(std::shared_ptr<SearchChessboard> cb) {
    //Should never be called
    return nullptr;
}

void
Human::moveEnded()
{
    // Nothing to do as a human
}

bool
Human::isHuman() {
    return true;
}
