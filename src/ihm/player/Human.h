#ifndef HUMAN_H
#define HUMAN_H

#include "IHMPlayer.h"
#include "../../chess/SearchChessboard.h"
#include "../../chess/SimpleMove.h"

#include <memory>
#include <SFML/Graphics.hpp>

class Human : public IHMPlayer
{
private:

public:
    Human();
    std::shared_ptr<SimpleMove> getNextMove(std::shared_ptr<SearchChessboard> cb) override;
    void moveEnded() override;
    bool isHuman() override;
};

#endif // HUMAN_H
