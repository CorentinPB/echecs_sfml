#ifndef AI_H
#define AI_H

#include "IHMPlayer.h"

#include <thread>
#include <mutex>

class AI : public IHMPlayer
{
private:
    int m_depth;
    std::shared_ptr<std::thread> m_thread = nullptr;
    std::mutex m_mutex;

    std::shared_ptr<SimpleMove> m_move = nullptr;

    bool m_isThreadRunning = false;

    void processBestMove(std::shared_ptr<SearchChessboard> cb);

public:
    AI();
    AI(int depth);

    std::shared_ptr<SimpleMove> getNextMove(std::shared_ptr<SearchChessboard> cb) override;
    void moveEnded() override;
    bool isHuman() override;
};

#endif // AI_H
