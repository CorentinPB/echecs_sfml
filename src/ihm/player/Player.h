/** 
 * Header de Player.cpp
 *
 * @file Player.h
 */

#if !defined Player_h
#define Player_h

#include "../../Commun.h"
#include "../../chess/SimplePiece.h"
#include "../ui_element/PiecesImpl.h"

#include <vector>
#include <memory>

class SearchChessboard;

class Player {
protected:
    std::vector<std::shared_ptr<Piece>> m_pieces;
    std::vector<std::shared_ptr<SimplePiece>> m_simplePieces;
public:
    explicit Player(const Color &white);
    virtual ~Player();
    virtual bool isWhite()=0; // pure virtual method
    void show() const;
    void placePieces(std::shared_ptr<SearchChessboard> e);
    void deletePieces(std::shared_ptr<Piece>);
    std::vector<std::shared_ptr<Piece>> getPieces();
    std::vector<std::shared_ptr<SimplePiece>> getSimplePieces();
};

class WhitePlayer : public Player
{
public:
    WhitePlayer();
    bool isWhite();
};

class BlackPlayer : public Player
{
public:
    BlackPlayer();
    bool isWhite();
};

#endif /* Player_h */
