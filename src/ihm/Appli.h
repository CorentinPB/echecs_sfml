/**
* Header of Appli.cpp
*
* @file Appli.h
*/

#ifndef APPLI_H
#define APPLI_H

#include "../chess/SearchChessboard.h"
#include "../chess/SearchChessboard.h"
#include "player/Player.h"
#include "../chess/TreeAI.h"
#include "player/Human.h"
#include "player/AI.h"
#include "ui_element/ButtonList.h"
#include "ui_element/HistoryList.h"

#include <SFML/Graphics.hpp>
#include <thread>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <array>
#include <memory>

class Appli
{
public:
    // Constructor
    Appli();
    // Destructor
    ~Appli();
    // Function
    void run();

    // Get the pixel position to draw a piece with an index
    static sf::Vector2f getDrawPos(unsigned int index);

private:
    sf::Font m_font;           // Font used with all the texts
    sf::Text end_game_text;    // Text dispayed when the game is ended
    std::thread* m_animThread; // Thread used to animate piece movements
    bool m_inAnimation;        // Bool used to know when an animation is playing
    bool m_drawPossibilities;  // Bool used to know when movement possibilites are displayed

    unsigned int m_actualPlayer; // int of the actual player

    bool m_inGame;  // If a game has started
    bool m_ai_selected; // If one AI player is selected

    bool m_appli_running; // If the application is running

    sf::RenderWindow    m_window;           // Application window

    sf::Vector2f        m_mouse;            // Mouse position

    std::shared_ptr<SearchChessboard> m_chessboard; // The main chessboard

    std::vector<ButtonList*>    m_button_lists; // Vector of all radioButton lists

    std::vector<Button*> m_buttons; // Vector of all buttons used in the game (radio buttons excluded)

    std::vector<sf::Text> m_text_list;  // Vector of all texts displayed in the game

    std::vector<sf::Text> m_text_ai_list;  // Vector of all texts related to the ai difficulty displayed in the game

    HistoryList* m_history_list;    // Contains all the history lines (up to five)

    sf::Text m_history_text;    // The "History" text displayed above the history lines

    std::vector<std::shared_ptr<HistoryElement>> m_history; // Vector of all history lines (not UI)

    std::array<std::unique_ptr<Player>,2> m_players;    // Array of all players
    std::array<std::unique_ptr<IHMPlayer>,2> m_IHMplayers;  // Array of all UI players

    std::string getPieceFile(std::string name, bool isWhite);   // Function used to get the file string from a piece name and it's color.

    std::shared_ptr<Piece> m_selected_piece;    // The current selected piece
    std::vector<unsigned int> m_selected_piece_movement_list;   // Vector of all of the current piece movements that are possible
    sf::CircleShape m_validMove;    // Shape displayed when a movement is valid (visual feedback

    //Functions

    void draw();        // draw
    void update();      // update

    void drawChessboard(sf::RenderWindow &w);
    void movePiece(sf::Vector2f dest, sf::Sprite &p);
    void selectPiece(sf::Sprite &p);
    void unselectPiece(sf::Sprite &p);

    std::shared_ptr<Piece> getPieceAtPos(unsigned int index);   // Used to get a piece from an index

    void keyPressed(sf::Keyboard::Key code);
    void keyReleased(sf::Keyboard::Key code);
    void mousePressed();
    void mouseReleased();
    void mouseMoved();

    void process_events();
    void processAI();
};

#endif // APPLI_H
