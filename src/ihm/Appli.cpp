/**
 * Implementation of Appli.h
 *
 * @file Appli.cpp
 */
#include "Appli.h"

const std::chrono::milliseconds ANIMATION_THREAD_TICK_TIME = std::chrono::milliseconds(1);

float clamp(float val, float min, float max) {
    if(val>max) return max;
    else if(val<min) return min;
    return val;
}

Appli::Appli() {
    m_animThread = nullptr;
    m_inAnimation = false;
    m_drawPossibilities = false;

    m_selected_piece = nullptr;
    m_chessboard = nullptr;
    m_players = {nullptr, nullptr};
    m_IHMplayers = {nullptr, nullptr};
    
    m_inGame = false;
    m_ai_selected = false;

    m_font.loadFromFile("ressources/chesterSans.ttf");
    end_game_text.setPosition(0,0);
    end_game_text.setCharacterSize(40);
    end_game_text.setFont(m_font);
    end_game_text.setFillColor(sf::Color::Black);
    end_game_text.setString("");

    int radius = (TILE_SIZE/2)/2;
    m_validMove.setRadius(radius);
    m_validMove.setOrigin(radius,radius);
    m_validMove.setOutlineThickness(5);
    m_validMove.setFillColor(sf::Color(0, 204, 0, 150));
    m_validMove.setOutlineColor(sf::Color(255, 255, 255, 150));

    m_chessboard = std::make_shared<SearchChessboard>(SearchChessboard(true));
    m_actualPlayer = m_chessboard->getActualPlayer();

    m_players[0] = std::unique_ptr<Player>(new WhitePlayer);
    m_players[1] = std::unique_ptr<Player>(new BlackPlayer);

    for(unsigned int i = 0; i < 2; ++i) {
        m_players[i]->placePieces(m_chessboard);
        for (std::shared_ptr<Piece> piece : m_players[i]->getPieces())
            piece->initSprite();
    }

    ButtonList* buttonList1 = new ButtonList();
    ButtonList* buttonList2 = new ButtonList();
    ButtonList* buttonList3 = new ButtonList();

    Button* button = new Button(100,350,"ressources/images/radioButtonSelected.png");
    buttonList1->addButton(button);
    Button* button2 = new Button(230,350,"ressources/images/radioButtonEmpty.png");
    buttonList1->addButton(button2);

    button = new Button(100,450,"ressources/images/radioButtonSelected.png");
    buttonList2->addButton(button);
    button2 = new Button(230,450,"ressources/images/radioButtonEmpty.png");
    buttonList2->addButton(button2);

    button = new Button(75,550,"ressources/images/radioButtonSelected.png");
    buttonList3->addButton(button);
    button2 = new Button(175,550,"ressources/images/radioButtonEmpty.png");
    buttonList3->addButton(button2);
    Button* button3 = new Button(275,550,"ressources/images/radioButtonEmpty.png");
    buttonList3->addButton(button3);

    buttonList1->setSelectedButton(0);
    buttonList2->setSelectedButton(0);
    buttonList3->setSelectedButton(0);

    m_button_lists.push_back(buttonList1);
    m_button_lists.push_back(buttonList2);
    m_button_lists.push_back(buttonList3);

    sf::Text text;

    text.setPosition(100,310);
    text.setCharacterSize(20);
    text.setFont(m_font);
    text.setFillColor(sf::Color::Black);
    text.setString("Joueur Blanc");

    m_text_list.push_back(text);

    text.setPosition(100,410);
    text.setString("Joueur Noir");

    m_text_list.push_back(text);

    text.setPosition(75,510);
    text.setString("Difficulte IA");

    m_text_ai_list.push_back(text);

    text.setPosition(135,350);
    text.setString("Humain");

    m_text_list.push_back(text);

    text.setPosition(265,350);
    text.setString("IA");

    m_text_list.push_back(text);

    text.setPosition(135,450);
    text.setString("Humain");

    m_text_list.push_back(text);

    text.setPosition(265,450);
    text.setString("IA");

    m_text_list.push_back(text);

    text.setPosition(110,550);
    text.setString("Facile");

    m_text_ai_list.push_back(text);

    text.setPosition(210,550);
    text.setString("Moyen");

    m_text_ai_list.push_back(text);

    text.setPosition(310,550);
    text.setString("Difficile");

    m_text_ai_list.push_back(text);

    Button* b = new Button(120, 600, "ressources/images/regularButtonEmpty.png", "Lancer");
    m_buttons.push_back(b);
    b = new Button(50, 800, "ressources/images/regularButtonEmpty.png", "Relancer");
    m_buttons.push_back(b);

    text.setPosition(1235,260);
    text.setString("Historique");

    m_history_text = text;

    m_history_list = new HistoryList();

}

Appli::~Appli() {

}

void
Appli::update() {

}

void
Appli::run() {
    m_window.create(sf::VideoMode{ WIDTH,HEIGHT,32 },
                    "Chess Game by COLIN/DUMARTIN/PACAUD",
                    sf::Style::Close
                    );

    m_window.setFramerateLimit(60);

    m_appli_running = true;
    while (m_appli_running) {
        process_events();
        if (m_inGame) {
            processAI();

            update();
        }
        draw();
    }
}

void
Appli::draw() {
    m_window.clear(sf::Color(150, 150, 150));

    drawChessboard(m_window);
    m_window.draw(end_game_text);
    m_window.draw(m_history_text);
    m_history_list->draw_to(m_window);

    for(unsigned int i = 0; i < 2; ++i)
        for (std::shared_ptr<Piece> piece : m_players[i]->getPieces())
            m_window.draw(piece->getSprite());

    if(m_selected_piece != nullptr && m_drawPossibilities) {
        for(unsigned int index : m_selected_piece_movement_list) {
            m_validMove.setPosition(getDrawPos(index));
            m_window.draw(m_validMove);
        }
    }

    if (!m_inGame) {
        for (int i = 0; i < m_button_lists.size(); i++) {
            if (i != 2) {
                if (m_button_lists[i]->getSelectedButton() == 1) {
                    m_ai_selected = true;
                }
            }
            if (i == 2) {
                if (m_ai_selected)
                    m_button_lists[2]->draw_to(m_window);
            } else {
                m_button_lists[i]->draw_to(m_window);
            }
        }

        for (sf::Text t : m_text_list) {
            m_window.draw(t);
        }
        // We draw the "launch" button only not in game
        m_buttons[0]->draw_to(m_window);

        if (m_ai_selected) {
            for (sf::Text t : m_text_ai_list) {
                m_window.draw(t);
            }
        }
    } else {
        // We draw the "restart" button only in game
        m_buttons[1]->draw_to(m_window);
    }
    m_window.display();
}

void
Appli::keyPressed(sf::Keyboard::Key code) {
    switch (code) {
    case sf::Keyboard::Escape:
        m_appli_running = false;
        break;
    case sf::Keyboard::Up:
        break;
    case sf::Keyboard::Down:
        break;
    case sf::Keyboard::Right:
        break;
    case sf::Keyboard::Left:
        break;
    case sf::Keyboard::A:
        break;
    case sf::Keyboard::Q:
        break;
    case sf::Keyboard::R:
        break;
    case sf::Keyboard::Z:
        break;
    case sf::Keyboard::S:
        break;
    default:
        break;
    }
}

void
Appli::keyReleased(sf::Keyboard::Key code) {
    switch (code) {
    case sf::Keyboard::Up:
        break;
    case sf::Keyboard::Down:
        break;
    case sf::Keyboard::Right:
        break;
    case sf::Keyboard::Left:
        break;
    default:
        break;
    }
}

void
Appli::mousePressed() {
    if (m_inGame) {
        for (int i = 0; i < m_buttons.size(); i++) {
            if (m_buttons[i]->contains(m_mouse)) {
                // If the selected button is clicked, we change his UI status
                m_buttons[i]->setStatus(2);
                // If the selected button is the restart button, we re-initialise the chessboard, pieces and players and we launch the option panel.
                if( i == 1 ) {
                    m_animThread->join();

                    m_chessboard = std::make_shared<SearchChessboard>(SearchChessboard(true));
                    m_actualPlayer = m_chessboard->getActualPlayer();

                    m_players[0] = std::unique_ptr<Player>(new WhitePlayer);
                    m_players[1] = std::unique_ptr<Player>(new BlackPlayer);

                    for(unsigned int i = 0; i < 2; ++i) {
                        m_players[i]->placePieces(m_chessboard);
                        for (std::shared_ptr<Piece> piece : m_players[i]->getPieces())
                            piece->initSprite();
                    }

                    end_game_text.setString("");

                    m_history_list->clear();

                    m_inGame = false;
                }
            }
        }
        if(!m_inAnimation && m_IHMplayers[m_actualPlayer]->isHuman()) {
            float x = (m_mouse.x - WIDTH/2 + TILE_SIZE*4)/TILE_SIZE;
            float y = (m_mouse.y - TILE_SIZE)/TILE_SIZE;
            if(x >= CHESSBOARD_LINE_SIZE || x < 0) {
                x = -1;
                y = -1;
            } else if(y >= CHESSBOARD_LINE_SIZE || y < 0) {
                y = -1;
                x = -1;
            }

            if(x != -1 && y != -1) {
                sf::Vector2f dest(x, y);
                unsigned int index = toIndex(x, y);
                if(m_selected_piece != nullptr) {
                    if ( std::find(m_selected_piece_movement_list.begin(), m_selected_piece_movement_list.end(), index) != m_selected_piece_movement_list.end() ) {
                        m_animThread = new std::thread(&Appli::movePiece, this, dest, std::ref(m_selected_piece->getSprite()));
                        m_drawPossibilities = false;
                    }
                    else if (m_selected_piece == Appli::getPieceAtPos(index)) {
                        m_animThread = new std::thread(&Appli::unselectPiece, this, std::ref(m_selected_piece->getSprite()));
                        m_selected_piece = nullptr;
                    }
                    else {
                        if(Appli::getPieceAtPos(index) == nullptr) {
                            m_animThread = new std::thread(&Appli::unselectPiece, this, std::ref(m_selected_piece->getSprite()));
                            m_selected_piece = nullptr;
                        }
                        else {
                            std::shared_ptr<Piece> tmp = Appli::getPieceAtPos(index);
                            if(m_players[m_actualPlayer]->isWhite() == tmp->isWhite()) {
                                m_animThread = new std::thread(&Appli::unselectPiece, this, std::ref(m_selected_piece->getSprite()));
                                m_selected_piece = tmp;
                                m_selected_piece_movement_list = m_selected_piece->movementsList(m_chessboard);
                                m_animThread = new std::thread(&Appli::selectPiece, this, std::ref(m_selected_piece->getSprite()));
                            }
                        }
                    }
                }
                else {
                    std::shared_ptr<Piece> tmp = Appli::getPieceAtPos(index);
                    if( tmp != nullptr && (m_players[m_actualPlayer]->isWhite() == tmp->isWhite() ) ) {
                        m_selected_piece = tmp;
                        m_animThread = new std::thread(&Appli::selectPiece, this, std::ref(m_selected_piece->getSprite()));
                        m_drawPossibilities = true;
                        m_selected_piece_movement_list = m_selected_piece->movementsList(m_chessboard);
                    }
                }
            }
            else if(m_selected_piece != nullptr) {
                m_animThread = new std::thread(&Appli::unselectPiece, this, std::ref(m_selected_piece->getSprite()));
                m_selected_piece = nullptr;
                m_drawPossibilities = false;
            }
        }
    } else {
        // If a radio button has been clicked, we change it's status (to selected)
        for (ButtonList* bl : m_button_lists) {
            for (int i = 0; i < bl->getButtonList().size(); i++) {
                if ((i != 2) || (i == 2 & m_ai_selected)) {
                    if (bl->getButtonList()[i]->contains(m_mouse)) {
                        bl->setSelectedButton(i);
                    }
                }
            }
        }

        bool tempAISelected = false;

        for (int i = 0; i < m_button_lists.size(); i++) {
            for (int j = 0; j < m_button_lists[i]->getButtonList().size(); j++) {
                if (i != 2) {
                    if (m_button_lists[i]->getSelectedButton() == 1) {
                        tempAISelected = true;
                    }
                }
            }
        }

        if (tempAISelected)
            m_ai_selected = true;
        else
            m_ai_selected = false;


        for (int i = 0; i < m_buttons.size(); i++) {
            if (m_buttons[i]->contains(m_mouse)) {
                m_buttons[i]->setStatus(2);
                switch (i) {
                // If the selected button is the launch one, we set the players options with the different radio buttons
                case 0:
                    if (!m_inGame) {
                        m_inGame = true;
                        switch (m_button_lists[0]->getSelectedButton()) {
                        case 0:
                            m_IHMplayers[0] = std::unique_ptr<IHMPlayer>(new Human());
                            break;
                        case 1:
                            switch (m_button_lists[2]->getSelectedButton()) {
                            case 0:
                                m_IHMplayers[0] = std::unique_ptr<IHMPlayer>(new AI(1));
                                break;
                            case 1:
                                m_IHMplayers[0] = std::unique_ptr<IHMPlayer>(new AI(3));
                                break;
                            case 2:
                                m_IHMplayers[0] = std::unique_ptr<IHMPlayer>(new AI(5));
                                break;
                            }
                            break;
                        }
                        switch (m_button_lists[1]->getSelectedButton()) {
                        case 0:
                            m_IHMplayers[1] = std::unique_ptr<IHMPlayer>(new Human());
                            break;
                        case 1:
                            switch (m_button_lists[2]->getSelectedButton()) {
                            case 0:
                                m_IHMplayers[1] = std::unique_ptr<IHMPlayer>(new AI(1));
                                break;
                            case 1:
                                m_IHMplayers[1] = std::unique_ptr<IHMPlayer>(new AI(3));
                                break;
                            case 2:
                                m_IHMplayers[1] = std::unique_ptr<IHMPlayer>(new AI(5));
                                break;
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }
}


void
Appli::mouseReleased() {
    // We set the correct status (visual feedback)
    for (Button* b : m_buttons) {
        if (b->contains(m_mouse)) {
            b->setStatus(1);
        }
    }
}

void
Appli::mouseMoved() {
    //For each buttonList, we update the correct status if the mouse is over the button or not (depending if the button is selected or not)
    for (ButtonList* bl : m_button_lists) {
        for (int i = 0; i < bl->getButtonList().size(); i++) {
            if (bl->getButtonList()[i]->contains(m_mouse)) {
                if (bl->getButtonList()[i]->isSelected()) {
                    bl->getButtonList()[i]->setStatus(3);
                } else {
                    bl->getButtonList()[i]->setStatus(2);
                }
            } else {
                if (bl->getButtonList()[i]->isSelected()) {
                    bl->getButtonList()[i]->setStatus(1);
                } else {
                    bl->getButtonList()[i]->setStatus(0);
                }
            }
        }
    }

    // Same goes for buttons, but we just need to set the status if the mouse is over the button
    for (Button* b : m_buttons) {
        if (b->contains(m_mouse)) {
            b->setStatus(1);
        } else {
            b->setStatus(0);
        }
    }
}

void
Appli::process_events() {
    sf::Event event;
    while (m_window.pollEvent(event)) {
        switch (event.type) {
        case sf::Event::Closed:
            m_appli_running = false;
            break;
        case sf::Event::KeyPressed:
            keyPressed(event.key.code);
            break;
        case sf::Event::KeyReleased:
            keyReleased(event.key.code);
            break;
        case sf::Event::MouseButtonPressed:
            mousePressed();
            break;
        case sf::Event::MouseButtonReleased:
            mouseReleased();
            break;
        case sf::Event::MouseMoved:
            m_mouse = m_window.mapPixelToCoords({ event.mouseMove.x,
                                                  event.mouseMove.y });
            mouseMoved();
            break;
        default:
            break;
        }
    }
}

void
Appli::processAI() {
    unsigned int player = m_actualPlayer;
    if (!m_inAnimation && !m_IHMplayers[player]->isHuman())
    {
        std::shared_ptr<SimpleMove> move = m_IHMplayers[player]->getNextMove(m_chessboard);
        if ( move != nullptr)
        {
            if(m_selected_piece != nullptr)
            {
                m_drawPossibilities = false;
                unsigned int x = IndexTools::getX(move->to());
                unsigned int y = IndexTools::getY(move->to());
                m_animThread = new std::thread(&Appli::Appli::movePiece, this, sf::Vector2f(x, y), std::ref(m_selected_piece->getSprite()));
                m_IHMplayers[player]->moveEnded();
            }
            else
            {
                m_drawPossibilities = false;
                m_selected_piece = Appli::getPieceAtPos(move->from());
                m_animThread = new std::thread(&Appli::Appli::selectPiece, this, std::ref(m_selected_piece->getSprite()));
            }
        }
    }
}

void
Appli::drawChessboard(sf::RenderWindow &w) {
    int border = TILE_SIZE/3;
    sf::RectangleShape back;
    back.setSize(sf::Vector2f(TILE_SIZE*8+border*2, TILE_SIZE*8+border*2));
    back.setPosition(WIDTH/2-TILE_SIZE*4-border, TILE_SIZE-border);
    back.setFillColor(sf::Color(51,51,51));
    w.draw(back);
    sf::RectangleShape tile;
    tile.setSize(sf::Vector2f(TILE_SIZE, TILE_SIZE));
    for(int i = 0; i < CHESSBOARD_LINE_SIZE ; ++i) {
        for(int j = 0; j < CHESSBOARD_LINE_SIZE ; ++j) {
            if((i+j)%2 == 0)
                tile.setFillColor(sf::Color(241, 186, 121));
            else
                tile.setFillColor(sf::Color(133, 65, 18));
            tile.setPosition(WIDTH/2-TILE_SIZE*4 + TILE_SIZE*i, TILE_SIZE + TILE_SIZE*j);
            w.draw(tile);
        }
    }
}

sf::Vector2f
Appli::getDrawPos(unsigned int index) {
    int x = IndexTools::getX(index);
    int y = IndexTools::getY(index);
    return sf::Vector2f(WIDTH/2-TILE_SIZE*3.5 + TILE_SIZE*x, TILE_SIZE*1.5 + TILE_SIZE*y);
}

float easeInOutCubic(float t, float b, float c, float d) {
    float tmp_t = t / (d / 2);
    if (tmp_t < 1)
        return (c/2*tmp_t*tmp_t*tmp_t) + b;
    tmp_t -= 2;
    return c / 2 * (tmp_t * tmp_t * tmp_t + 2) + b;
}

void
Appli::movePiece(sf::Vector2f dest, sf::Sprite &p) {
    GLOBAL_MUTEX.lock();
    m_inAnimation = true;
    float time = 0;
    float duration = 1;
    int startX = p.getPosition().x;
    int startY = p.getPosition().y;
    int changeX = getDrawPos(toIndex(dest.x, dest.y)).x-startX;
    int changeY = getDrawPos(toIndex(dest.x, dest.y)).y-startY;
    while(time<duration ) {
        int x = easeInOutCubic(time,startX,changeX, duration);
        int y = easeInOutCubic(time,startY,changeY, duration);
        p.setPosition(x, y);
        std::this_thread::sleep_for(ANIMATION_THREAD_TICK_TIME);
        time += 0.016;
    }

    unsigned int dest_index = toIndex(dest.x, dest.y);

    std::shared_ptr<Piece> pieceToDelete = getPieceAtPos(dest_index);
    if(pieceToDelete != nullptr) {
        if(m_actualPlayer == 0)
            m_players[1]->deletePieces(pieceToDelete);
        else
            m_players[0]->deletePieces(pieceToDelete);
    }
    m_chessboard->move(m_selected_piece->getSimplePiece()->getIndex(), dest_index);

    // Temporary history element used to store the informations of the last move
    std::shared_ptr<HistoryElement> tempHistoryElement = m_chessboard->getLastHistoryElement();

    // We need to convert the char var to a string
    std::string movedPiece(1, m_selected_piece->getChar());

    GLOBAL_MUTEX.unlock();
    unselectPiece(p);
    GLOBAL_MUTEX.lock();
    m_inAnimation = true;

    if (tempHistoryElement->getEaten() != nullptr) {
        std::string takenPiece(1, tempHistoryElement->getEaten()->getChar());
        m_history_list->addHistoryLine(tempHistoryElement->getFrom(),tempHistoryElement->getTo(), getPieceFile(movedPiece, m_selected_piece->isWhite()), getPieceFile(takenPiece,tempHistoryElement->getEaten()->isWhite()));
    } else {
        m_history_list->addHistoryLine(tempHistoryElement->getFrom(),tempHistoryElement->getTo(), getPieceFile(movedPiece, m_selected_piece->isWhite()), "");
    }

    m_selected_piece = nullptr;
    m_actualPlayer = !m_actualPlayer;

    // If the game is ended, we update the text with the correct string.
    unsigned int stale_checkmate = m_chessboard->isCheckmateOrStalemate(m_actualPlayer);
    if(stale_checkmate == 1) {
        end_game_text.setString("Pat, Egalité");
    } else if(stale_checkmate == 2) {
        end_game_text.setString("Echec et Mat du joueur "+((m_actualPlayer==0)?std::string("Blanc"):std::string("Noir")));
    } else if(m_chessboard->isCheck(m_actualPlayer)) {
        end_game_text.setString("Joueur "+((m_actualPlayer==0)?std::string("Blanc"):std::string("Noir"))+" en Echec");
    } else {
        end_game_text.setString("");
    }
    m_inAnimation = false;
    GLOBAL_MUTEX.unlock();
}

std::string Appli::getPieceFile(std::string name,bool isWhite) {
    if (name == "R" || name == "r") {
        if (isWhite)
            return "ressources/images/white_king.png";
        else
            return "ressources/images/black_king.png";
    } else if (name == "P" || name == "p"){
        if (isWhite)
            return "ressources/images/white_pawn.png";
        else
            return "ressources/images/black_pawn.png";
    } else if (name == "C" || name == "c"){
        if (isWhite)
            return "ressources/images/white_knight.png";
        else
            return "ressources/images/black_knight.png";
    } else if (name == "F" || name == "f"){
        if (isWhite)
            return "ressources/images/white_bishop.png";
        else
            return "ressources/images/black_bishop.png";
    } else if (name == "T" || name == "t"){
        if (isWhite)
            return "ressources/images/white_tower.png";
        else
            return "ressources/images/black_tower.png";
    } else if (name == "Q" || name == "q"){
        if (isWhite)
            return "ressources/images/white_queen.png";
        else
            return "ressources/images/black_queen.png";
    }
}

void
Appli::selectPiece(sf::Sprite &p) {
    GLOBAL_MUTEX.lock();
    m_inAnimation = true;
    float time = 0;
    float duration = 0.3f;
    while(time<duration ) {
        float scale = easeInOutCubic(time,1,0.4f, duration);
        p.setScale(scale, scale);
        std::this_thread::sleep_for(ANIMATION_THREAD_TICK_TIME);
        time += 0.016;
    }
    m_inAnimation = false;
    GLOBAL_MUTEX.unlock();
}

void
Appli::unselectPiece(sf::Sprite &p) {
    GLOBAL_MUTEX.lock();
    m_inAnimation = true;
    float time = 0;
    float duration = 0.3f;
    while(time<duration ) {
        float scale = easeInOutCubic(time,1.4f,-0.4f, duration);
        p.setScale(scale, scale);
        std::this_thread::sleep_for(ANIMATION_THREAD_TICK_TIME);
        time += 0.01;
    }
    m_inAnimation = false;
    GLOBAL_MUTEX.unlock();
}

std::shared_ptr<Piece>
Appli::Appli::getPieceAtPos(unsigned int index)
{
    for(unsigned int i = 0; i < 2; ++i)
        for (std::shared_ptr<Piece> p : m_players[i]->getPieces())
            if (p->index() == index)
                return p;
    return nullptr;
}
