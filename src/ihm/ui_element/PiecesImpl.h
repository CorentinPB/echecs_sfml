/**
 * Header of PieceImpl.cpp
 *
 * @file PieceImpl.h
 */
#ifndef PIECESIMPL_H
#define PIECESIMPL_H

class Chessboard;

#include "../../Commun.h"
#include "Piece.h"

#include <SFML/Graphics.hpp>
#include <vector>

class King : public Piece
{
public:
    King(const Color &white);
    char getChar() const;
};

class Pawn : public Piece
{
public:
    Pawn(const Color &white, unsigned int pos);
    char getChar() const;
};

class Knight : public Piece
{
public:
    Knight(const Color &white, const bool left);
    char getChar() const;
};

class Bishop : virtual public Piece
{
public:
    Bishop(const Color &white, const bool left);
    char getChar() const;
};

class Rook : virtual public Piece
{
public:
    Rook(const Color &white, bool left);
    char getChar() const;
};

class Queen : public Piece
{
public:
    Queen(const Color &white);
    char getChar() const;
};

#endif // PIECESIMPL_H
