/**
 * Implementation of PiecesImpl.h
 *
 * @file PiecesImpl.cpp
 */
#include "PiecesImpl.h"

#include "../Appli.h"
#include "Piece.h"
#include "../../chess/SimplePiecesImpl.h"

#include <iostream>
#include <string>
#include <memory>

const std::string IMG_DIR = "ressources/images/";
const std::string WHITE_BISHOP   =  IMG_DIR + "white_bishop.png";
const std::string WHITE_KING     =  IMG_DIR + "white_king.png";
const std::string WHITE_KNIGHT   =  IMG_DIR + "white_knight.png";
const std::string WHITE_PAWN     =  IMG_DIR + "white_pawn.png";
const std::string WHITE_QUEEN    =  IMG_DIR + "white_queen.png";
const std::string WHITE_TOWER    =  IMG_DIR + "white_tower.png";
const std::string BLACK_BISHOP   =  IMG_DIR + "black_bishop.png";
const std::string BLACK_KING     =  IMG_DIR + "black_king.png";
const std::string BLACK_KNIGHT   =  IMG_DIR + "black_knight.png";
const std::string BLACK_PAWN     =  IMG_DIR + "black_pawn.png";
const std::string BLACK_QUEEN    =  IMG_DIR + "black_queen.png";
const std::string BLACK_TOWER    =  IMG_DIR + "black_tower.png";


/****** Constructors used at init ******/

King::King(const Color &white) : Piece(std::make_shared<SimpleKing>(SimpleKing(white==Color::colorWhite)))
{
    if(white==Color::colorWhite) m_texture.loadFromFile(WHITE_KING);
    else m_texture.loadFromFile(BLACK_KING);
    m_sprite.setTexture(m_texture);
}


Pawn::Pawn(const Color &white, unsigned int pos) : Piece(std::make_shared<SimplePawn>(SimplePawn(white==Color::colorWhite, pos)))
{
    if(white==Color::colorWhite) m_texture.loadFromFile(WHITE_PAWN);
    else m_texture.loadFromFile(BLACK_PAWN);
    m_sprite.setTexture(m_texture);
}


Knight::Knight(const Color &white, const bool left) : Piece(std::make_shared<SimpleKnight>(SimpleKnight(white==Color::colorWhite, left)))
{
    if(white==Color::colorWhite) m_texture.loadFromFile(WHITE_KNIGHT);
    else m_texture.loadFromFile(BLACK_KNIGHT);
    m_sprite.setTexture(m_texture);
}


Bishop::Bishop(const Color &white, const bool left) : Piece(std::make_shared<SimpleBishop>(SimpleBishop(white==Color::colorWhite, left)))
{
    if(white==Color::colorWhite) m_texture.loadFromFile(WHITE_BISHOP);
    else m_texture.loadFromFile(BLACK_BISHOP);
    m_sprite.setTexture(m_texture);
}


Rook::Rook(const Color &white, const bool left) : Piece(std::make_shared<SimpleRook>(SimpleRook(white==Color::colorWhite, left)))
{
    if(white==Color::colorWhite) m_texture.loadFromFile(WHITE_TOWER);
    else m_texture.loadFromFile(BLACK_TOWER);
    m_sprite.setTexture(m_texture);
}


Queen::Queen(const Color &white) : Piece(std::make_shared<SimpleQueen>(SimpleQueen(white==Color::colorWhite)))
{
    if(white==Color::colorWhite) m_texture.loadFromFile(WHITE_QUEEN);
    else m_texture.loadFromFile(BLACK_QUEEN);
    m_sprite.setTexture(m_texture);
}

/****** King Methods ******/

char
King::getChar() const
{
    return (m_simplePiece->isWhite())?'R':'r';
}


/****** Pawn Methods ******/

char
Pawn::getChar() const
{
    return (m_simplePiece->isWhite())?'P':'p';
}


/****** Knight Methods ******/

char
Knight::getChar() const
{
    return (m_simplePiece->isWhite())?'C':'c';
}


/****** Bishop Methods ******/


char
Bishop::getChar() const
{
    return (m_simplePiece->isWhite())?'F':'f';
}


/****** Rook Methods ******/

char
Rook::getChar() const
{
    return (m_simplePiece->isWhite())?'T':'t';
}


/****** DA QUEEN Methods ******/

char
Queen::getChar() const
{
    return (m_simplePiece->isWhite())?'Q':'q';
}

