/** 
 * Header of Piece.cpp
 *
 * @file Piece.h
 */

#if !defined Piece_h
#define Piece_h

#include "../../Commun.h"
#include "../../chess/SimplePiece.h"

#include <SFML/Graphics.hpp>
#include <vector>

class Piece
{
protected:
    std::shared_ptr<SimplePiece> m_simplePiece; // piece variable
    sf::Sprite m_sprite;                        // button sprite
    sf::Texture m_texture;                      // texture of the button

public:
    Piece(std::shared_ptr<SimplePiece> sp);
    ~Piece();

    // Getters
    unsigned int index() const;
    bool isWhite() const;
    virtual char getChar() const;
    std::shared_ptr<SimplePiece> getSimplePiece() const;
    sf::Sprite & getSprite();

    void initSprite();
    void move(unsigned int index);
    void affiche() const;

    std::shared_ptr<Piece> clone();
    bool validMovement(std::shared_ptr<SearchChessboard> e, unsigned int index);
    std::vector<unsigned int> movementsList(std::shared_ptr<SearchChessboard> e);
}; 


#endif // !defined Piece_h
