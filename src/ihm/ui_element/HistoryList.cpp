#include "HistoryList.h"

HistoryList::HistoryList()
    : m_from_list({}),  m_to_list({}), m_eaten_elements({}), m_history_positions({}) {

    // We load all the lines positions
    m_history_positions.push_back(std::pair<int,int>(1200,300));
    m_history_positions.push_back(std::pair<int,int>(1200,360));
    m_history_positions.push_back(std::pair<int,int>(1200,420));
    m_history_positions.push_back(std::pair<int,int>(1200,480));
    m_history_positions.push_back(std::pair<int,int>(1200,540));

    // We push back nullptr to always have the same size in the vector
    m_eaten_elements.push_back(nullptr);
    m_eaten_elements.push_back(nullptr);
    m_eaten_elements.push_back(nullptr);
    m_eaten_elements.push_back(nullptr);
    m_eaten_elements.push_back(nullptr);

    m_font.loadFromFile("ressources/chesterSans.ttf");
}

// Called when a new move has been made
void HistoryList::addHistoryLine(int from, int to, std::string movedPiece, std::string takenPiece) {
    PictureElement* movedPiecePicture = new PictureElement();
    PictureElement* takenPiecePicture = new PictureElement();

    sf::Text from_text;
    from_text.setCharacterSize(20);
    from_text.setFont(m_font);
    from_text.setFillColor(sf::Color::Black);
    from_text.setString("x : " + std::to_string(IndexTools::getX(from)) + " | " + "y : " + std::to_string(IndexTools::getY(from)));

    sf::Text to_text;
    to_text.setCharacterSize(20);
    to_text.setFont(m_font);
    to_text.setFillColor(sf::Color::Black);
    to_text.setString("x : " + std::to_string(IndexTools::getX(to)) + " | " + "y : " + std::to_string(IndexTools::getY(to)));

    // If the list isn't full (only 5 lines at the same time until it replace the last entry)
    if (m_from_list.size() != 5) {
        from_text.setPosition(m_history_positions[m_from_list.size()].first + 50,m_history_positions[m_from_list.size()].second + 15);
        to_text.setPosition(m_history_positions[m_from_list.size()].first + 150,m_history_positions[m_from_list.size()].second + 15);
        movedPiecePicture->setPosition(m_history_positions[m_from_list.size()].first, m_history_positions[m_from_list.size()].second + 5);
    } else {
        from_text.setPosition(m_history_positions[4].first + 50,m_history_positions[4].second + 5);
        to_text.setPosition(m_history_positions[4].first + 150,m_history_positions[4].second + 5);
        movedPiecePicture->setPosition(m_history_positions[4].first, m_history_positions[4].second + 5);
    }
    movedPiecePicture->setPicture(movedPiece);

    // For all elements in the vector, we push them back one position back to erase the last entry and we update the UI position
    for (unsigned int i = 0; i < m_eaten_elements.size() - 1; i++) {
        m_eaten_elements[i] = m_eaten_elements[i + 1];
        if (m_eaten_elements[i] != nullptr)
            m_eaten_elements[i]->setPosition(m_history_positions[i].first + 240,m_history_positions[i].second + 5);
    }
    m_eaten_elements[4] = nullptr;

    // If the piece has taken another piece, we update the new entry with the taken piece picture
    if (takenPiece != "") {
        if (m_from_list.size() < 5) {
            takenPiecePicture->setPosition(m_history_positions[m_from_list.size()].first + 240, m_history_positions[m_from_list.size()].second + 5);
            m_eaten_elements[m_from_list.size()] = takenPiecePicture;
        } else {
            m_eaten_elements[4] = takenPiecePicture;
        }
        takenPiecePicture->setPicture(takenPiece);
        if (m_eaten_elements[4] != nullptr)
            m_eaten_elements[4]->setPosition(m_history_positions[4].first + 240,m_history_positions[4].second);
    }

    // If the list is full, we push back all the elements from the vector like we did with the eaten vector,
    // we update the UI position and we add the infos for the next line. If the list isn't full, we push back new
    // elements until it is.
    if (m_from_list.size() == 5) {
        for (unsigned int i = 0; i < m_from_list.size() - 1; i++) {
            m_from_list[i] = m_from_list[i + 1];
            m_to_list[i] = m_to_list[i + 1];
            m_moved_elements[i] = m_moved_elements[i + 1];
            m_from_list[i].setPosition(m_history_positions[i].first + 50,m_history_positions[i].second + 15);
            m_to_list[i].setPosition(m_history_positions[i].first + 150,m_history_positions[i].second + 15);
            m_moved_elements[i]->setPosition(m_history_positions[i].first,m_history_positions[i].second + 5);
        }
        m_from_list[4] = from_text;
        m_to_list[4] = to_text;
        m_moved_elements[4] = movedPiecePicture;
        m_moved_elements[4]->setPosition(m_history_positions[4].first,m_history_positions[4].second);
    } else {
        m_from_list.push_back(from_text);
        m_to_list.push_back(to_text);
        m_moved_elements.push_back(movedPiecePicture);
    }
}

void HistoryList::clear() {
    m_eaten_elements.clear();

    m_eaten_elements.push_back(nullptr);
    m_eaten_elements.push_back(nullptr);
    m_eaten_elements.push_back(nullptr);
    m_eaten_elements.push_back(nullptr);
    m_eaten_elements.push_back(nullptr);

    m_moved_elements.clear();
    m_from_list.clear();
    m_to_list.clear();
}


void HistoryList::draw_to(sf::RenderWindow &window) {
    for (PictureElement* element : m_eaten_elements) {
        if (element != nullptr)
            element->draw_to(window);
    }
    for (PictureElement* element : m_moved_elements) {
        element->draw_to(window);
    }
    for (sf::Text text : m_from_list) {
        window.draw(text);
    }
    for (sf::Text text : m_to_list) {
        window.draw(text);
    }
}
