#ifndef HISTORYLIST_H
#define HISTORYLIST_H

#include <SFML/Graphics.hpp>
#include "PictureElement.h"
#include "../../Commun.h"

class HistoryList
{
public:
    // Contructors
    HistoryList();

    void addHistoryLine(int from, int to, std::string movedPiece, std::string takenPiece);

    void clear();

    void draw_to(sf::RenderWindow &window);
private:
    sf::Font m_font;                                     // font of the displayed text
    std::vector<sf::Text>    m_from_list;                // list of texts that contains the case where the piece comes from
    std::vector<sf::Text>    m_to_list;                  // list of texts that contains the case where the piece goes to
    std::vector<PictureElement*>    m_eaten_elements;    // list of all reducted piece pictures (eaten pieces)
    std::vector<PictureElement*>    m_moved_elements;    // list of all reducted piece pictures (moved pieces)
    std::vector<std::pair<int,int>> m_history_positions; // list of all postitions where the lines are going to be displayed
};


#endif // HISTORYLIST_H
