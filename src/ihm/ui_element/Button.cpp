#include "Button.h"

Button::Button(const int &x, const int &y, const std::string &file)
    :m_fileName{file}
{
    m_with_text = false;
    setStatus(0);
    m_image.loadFromFile(m_fileName);
    m_image.setSmooth(1);
    m_sprite.setTexture(m_image);
    m_sprite.setPosition({ (float)x, (float)y});
}

Button::Button(const int &x, const int &y, const std::string &file, const std::string &text)
    :m_fileName{file}
{
    m_with_text = true;
    m_image.loadFromFile(m_fileName);
    m_image.setSmooth(1);
    m_sprite.setTexture(m_image);
    m_sprite.setPosition({ (float)x, (float)y});
    m_font.loadFromFile("ressources/chesterSans.ttf");
    m_text.setString(text);
    m_text.setFont(m_font);
    m_text.setColor(sf::Color::Black);
    m_text.setCharacterSize(getSize().y/2);
    m_text.setPosition({ (float)x + m_sprite.getGlobalBounds().width/2 - m_text.getGlobalBounds().width/2
                         ,(float)y + m_sprite.getGlobalBounds().height/2 - m_text.getGlobalBounds().height/2
                         + m_text.getPosition().y - m_text.getGlobalBounds().top});
}

bool Button::contains(const sf::Vector2f &vect) const {
    return m_sprite.getGlobalBounds().contains(vect);
}

void Button::changeTexture(const int &nb) {
    switch(nb) {
    // If the button isn't selected
    case 0:
        if (!m_with_text)
            m_image.loadFromFile("ressources/images/radioButtonEmpty.png");
        else
            m_image.loadFromFile("ressources/images/regularButtonEmpty.png");
        break;
    // If the button has been clicked/selected
    case 1:
        if (!m_with_text)
            m_image.loadFromFile("ressources/images/radioButtonClicked.png");
        else
            m_image.loadFromFile("ressources/images/regularButtonSelected.png");
        break;
    // If the button is clicked/selected
    case 2:
        if (!m_with_text)
            m_image.loadFromFile("ressources/images/radioButtonSelected.png");
        else
            m_image.loadFromFile("ressources/images/regularButtonClicked.png");
        break;
    // If the button is clicked and selected (radio button case)
    case 3:
        m_image.loadFromFile("ressources/images/radioButtonClickedSelected.png");
        break;
    }
    m_sprite.setTexture(m_image);
}

sf::Vector2i Button::getSize() const {
    return {m_sprite.getTextureRect().width, m_sprite.getTextureRect().height};
}

int Button::getStatus() const {
    return m_status;
}

bool Button::isSelected() const {
    return m_selected;
}

void Button::setStatus(const int &status) {
    changeTexture(status);
    if (!m_with_text) {
        if (status == 1 || status == 3) {
            m_selected = true;
        } else {
            m_selected = false;
        }
    }
    m_status = status;
}

void Button::draw_to(sf::RenderWindow &window) {
    if (m_with_text) {
        window.draw(m_text);
    }
    window.draw(m_sprite);
}
