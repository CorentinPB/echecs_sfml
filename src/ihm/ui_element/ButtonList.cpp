#include "ButtonList.h"

ButtonList::ButtonList() {

}

void ButtonList::addButton(Button* b) {
    m_buttons.push_back(b);
}

void ButtonList::setSelectedButton(const int &num) {
    m_selected_button = num;
    for (int i = 0; i < m_buttons.size(); i++) {
        if (i == m_selected_button) {
            m_buttons[i]->setStatus(3);
        } else {
            m_buttons[i]->setStatus(0);
        }
    }
}

int ButtonList::getSelectedButton() const {
    return m_selected_button;
}

std::vector<Button*> ButtonList::getButtonList() const {
    return m_buttons;
}

void ButtonList::draw_to(sf::RenderWindow &window) {
    for (Button* b : m_buttons) {
        b->draw_to(window);
    }
}
