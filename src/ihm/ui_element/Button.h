#ifndef BUTTON_H
#define BUTTON_H

#include <string>
#include <SFML/Graphics.hpp>

class Button
{
public:
    // Contructors
    Button(const int &x, const int &y, const std::string &file);
    Button(const int &x, const int &y, const std::string &file, const std::string &text);

    // Contains funtion
    bool contains(const sf::Vector2f &vect)         const;  // main contains

    // Texture change function
    void changeTexture(const int &nb);

    // Getters
    sf::Vector2i    getSize()       const;
    int             getStatus()     const;
    bool            isSelected()    const;

    // Setter
    void setStatus(const int &status);

    void draw_to(sf::RenderWindow &window);

private:
    bool m_with_text;

    sf::Font    m_font;     // font of the text
    sf::Text    m_text;     // text displayed in the button
    std::string m_fileName; // file name of the texture
    sf::Texture m_image;    // texture of the button
    sf::Sprite  m_sprite;   // button sprite
    int m_status;
    bool m_selected;
};

#endif // BUTTON_H
