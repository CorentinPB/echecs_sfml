#include "PictureElement.h"

PictureElement::PictureElement() {}

void PictureElement::setPosition(const int &x, const int &y) {
    m_x = x;
    m_y = y;
    m_sprite.setPosition({m_x, m_y});
}

void PictureElement::setPicture(const std::string &file) {
    m_fileName = file;
    m_image.loadFromFile(m_fileName);
    m_image.setSmooth(1);
    m_sprite.setTexture(m_image);
    m_sprite.setPosition({m_x, m_y});
    m_sprite.setScale(0.5,0.5);
}

void PictureElement::draw_to(sf::RenderWindow &window) {
    window.draw(m_sprite);
}
