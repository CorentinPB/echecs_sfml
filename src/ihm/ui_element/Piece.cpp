/**
 * Implementation of Piece.h
 *
 * @file Piece.cpp
 */
#include "Piece.h"

#include "../Appli.h"

#include <iostream>
#include <string>



Piece::Piece(std::shared_ptr<SimplePiece> sp) : m_simplePiece(sp)
{}

Piece::~Piece()
{}

void
Piece::move(unsigned int c )
{
    m_simplePiece->move(c);
}

unsigned int
Piece::index() const
{
    return m_simplePiece->getIndex();
}

bool
Piece::isWhite() const
{
    return m_simplePiece->isWhite();
}

void
Piece::affiche() const
{
    std::cout << "Piece: x=" << IndexTools::getY(m_simplePiece->getIndex()) << " y=" << IndexTools::getY(m_simplePiece->getIndex()) << " "
         << ( (m_simplePiece->isWhite()) ? "blanche" : "noire" ) << std::endl;
}

bool
Piece::validMovement(std::shared_ptr<SearchChessboard> e, unsigned int index)
{
    return m_simplePiece->validMovement(e , index);
}

std::vector<unsigned int>
Piece::movementsList(std::shared_ptr<SearchChessboard> e)
{
    return m_simplePiece->movementsIndexList(e);
}

sf::Sprite &
Piece::getSprite() {
    return m_sprite;
}

void
Piece::initSprite() {
    m_texture.setSmooth(true);
    m_sprite.setTexture(m_texture);
    m_sprite.setOrigin(m_sprite.getGlobalBounds().width/2, m_sprite.getGlobalBounds().height/2);
    m_sprite.setPosition(Appli::getDrawPos(m_simplePiece->getIndex()));
}

char
Piece::getChar() const
{
    return (m_simplePiece->isWhite())?'B':'N';
}

std::shared_ptr<SimplePiece>
Piece::getSimplePiece() const
{
    return m_simplePiece;
}
