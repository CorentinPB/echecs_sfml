#ifndef PICTUREELEMENT_H
#define PICTUREELEMENT_H

#include <string>
#include <SFML/Graphics.hpp>

class PictureElement
{
public:
    // Contructors
    PictureElement();

    // Setters
    void setPosition(const int &x, const int &y);
    void setPicture(const std::string &file);

    void draw_to(sf::RenderWindow &window);

private:
    float m_x;              // x position of the picture
    float m_y;              // y position of the picture
    std::string m_fileName; // file name of the texture
    sf::Texture m_image;    // texture of the button
    sf::Sprite  m_sprite;   // button sprite
};

#endif // PictureElement_H
