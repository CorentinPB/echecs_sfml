#ifndef BUTTONLIST_H
#define BUTTONLIST_H

#include <string>
#include <SFML/Graphics.hpp>

#include "Button.h"

class ButtonList
{
public:
    // Contructors
    ButtonList();

    void addButton(Button* b);

    // Setter
    void setSelectedButton(const int &num);

    // Getters
    int getSelectedButton() const;
    std::vector<Button*> getButtonList() const;

    void draw_to(sf::RenderWindow &window);
private:
    std::vector<Button*>    m_buttons;  // contains all radio buttons
    int m_selected_button;              // index of the selected button
};

#endif // BUTTONLIST_H
