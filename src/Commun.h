/**
 * @file Commun.h
 */

#if !defined Commun_h
#define Commun_h
#include <mutex>

enum class Color {
    colorNone,  // Represente l’absence de couleur
    colorWhite, // Represente la couleur blanche
    colorBlack, // Represente la couleur noire
    colorMax    // Valeur maximale a ne jamais atteindre
};

const int DEFAULT_VALUE = 99999;
const int DEPTH = 1;

const int WIDTH = 1600;
const int HEIGHT = 900;
const int TILE_SIZE = HEIGHT/10;

const unsigned int CHESSBOARD_SIZE = 64;
const unsigned int CHESSBOARD_LINE_SIZE = 8;

static std::mutex GLOBAL_MUTEX;

inline unsigned int toIndex(unsigned int x, unsigned int y) {return x+(CHESSBOARD_LINE_SIZE*y);}
inline bool isValid(unsigned int x, unsigned int y) {return x<CHESSBOARD_LINE_SIZE && y<CHESSBOARD_LINE_SIZE;}

class IndexTools {
public:
    static bool isValid(unsigned int index) {return (index < CHESSBOARD_SIZE);}
    static unsigned int getX(unsigned int index) {return (index%CHESSBOARD_LINE_SIZE);}
    static unsigned int getY(unsigned int index) {return (index/CHESSBOARD_LINE_SIZE);}
};


#endif
